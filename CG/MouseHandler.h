#ifndef MouseHandler_H
#define MouseHandler_H

#include "InputHandler.h"

class MouseHandler : public virtual InputHandler
{
public:
	MouseHandler()
	{
		Application::getInstance().AddHandler(this);
	}

	glm::vec2 GetMousePosition() { return Application::getInstance().GetMousePosition(); }
	glm::vec2 GetMousePositionRelative() { return Application::getInstance().GetMousePositionRelative(); }

	virtual void OnLBUp() {}
	virtual void OnLBDown() {}

	virtual void OnRBUp() {}
	virtual void OnRBDown() {}

	virtual void OnMBUp() {}
	virtual void OnMBDown() {}

	virtual void OnMouseMove(int x, int y) {}
};

#endif