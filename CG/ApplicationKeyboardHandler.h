#ifndef ApplicationKeyboardHandler_h__
#define ApplicationKeyboardHandler_h__

#include "KeyboardHandler.h"
#include "Application.h"
#include "SceneObject.h"
#include "Camera.h"
#include <string>
#include "GuidedCamera.h"

class ApplicationKeyboardHandler : public KeyboardHandler, public SceneObject
{
public:

	ApplicationKeyboardHandler(Application& target)
		: mTarget(target) { }

	virtual void OnKeyDown(unsigned char key)
	{
		KeyboardHandler::OnKeyDown(key);
	}

	virtual void OnKeyUp(unsigned char key)
	{
		KeyboardHandler::OnKeyUp(key);

		if (key == 'c') 
		{
			mTarget.Stop();
		}
		else if (key == 'f')
		{
			std::string tmp = mTarget.GetScene()->GetActiveCamera()->GetName();

			if (tmp == "free")
			{
				mTarget.GetScene()->SetActiveCamera("rotation");
			}
			else if (tmp == "rotation")
			{
				mTarget.GetScene()->SetActiveCamera("guided");
				((GuidedCamera*)mTarget.GetScene()->GetActiveCamera())->GetPathAnimation()->Start(true);
			}
			else if (tmp == "guided")
			{
				((GuidedCamera*)mTarget.GetScene()->GetActiveCamera())->GetPathAnimation()->Stop();
				mTarget.GetScene()->SetActiveCamera("free");
			}
		}
		else if (key == 'n')
		{
			if (std::string("free") == mTarget.GetScene()->GetActiveCamera()->GetName())
			{
				auto guidedCam = (GuidedCamera*)mTarget.GetScene()->GetCamera("guided");
				guidedCam->GetPathAnimation()->Stop();
				guidedCam->GetPathAnimation()->Clear();

				printf("Path cleared\n");
			}
		}
		else if (key == 'p')
		{
			if (std::string("free") == mTarget.GetScene()->GetActiveCamera()->GetName())
			{
				auto freeCam = mTarget.GetScene()->GetActiveCamera();
				auto guidedCam = (GuidedCamera*)mTarget.GetScene()->GetCamera("guided");

				guidedCam->GetPathAnimation()->Stop();
				guidedCam->GetPathAnimation()->AppendPoint(2.0f, freeCam->GetEyePos(), freeCam->GetLookAt());
			}
		}
	}

	virtual void OnAttach()
	{

	}

	virtual void OnRender()
	{

	}

	virtual void OnUpdate()
	{

	}

private:
	Application& mTarget;
};

#endif // ApplicationKeyboardHandler_h__