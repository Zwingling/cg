/* 
 * File:   Application.h
 * Author: mathias
 *
 * Created on March 22, 2015, 5:42 PM
 */

#ifndef APPLICATION_H
#define	APPLICATION_H

#include <cstdlib>
#include <string>
#include <vector>
#include "glm/glm.hpp"

#include <GL/glew.h>
#include <GL/freeglut.h>

#include "Timer.h"

class Scene;
class KeyboardHandler;
class MouseHandler;

class Application 
{
private:
    unsigned int mWindowWidth;
    unsigned int mWindowHeight;
	bool mbPaused;
    std::string mWindowTitle;
    Scene* mScene;
	Timer mTimer;
	unsigned int mFPS;
	double mRefreshTime;
	bool mUseFixRefreshRate;
	float mElapsed;
	bool mWarped;

	bool mStop;
	bool mFirstInput;
	glm::vec2 mMousePosition;
	glm::vec2 mMousePositionRelative;
	std::vector<KeyboardHandler*> mKeyboardHandler;
	std::vector<MouseHandler*> mMouseHandler;
	 
	GLuint mRenderTarget;
	GLuint mRenderTargetFBO;

    Application();
    ~Application();
    Application(Application const&); // Don't Implement
    void operator=(Application const&); // Don't implement
    
public:
    static Application& getInstance() {
        static Application instance;
        return instance; 
    }

	int GetWindowWidth()
	{
		return mWindowWidth;
	}

	int GetWindowHeight()
	{
		return mWindowHeight;
	}

	GLuint GetRenderTarget()
	{
		return mRenderTarget;
	}

    Scene* GetScene() {
        return mScene;
    }

	float GetAspectRatio()
	{
		return mWindowWidth / (float)mWindowHeight;
	}

    void Initialize(unsigned int windowWidth, unsigned int windowHeight, std::string windowTitle);
    void Start();
	void Stop();
    void OnRender();
	void OnUpdate();
	void OnMouse(int button, int state, int x, int y);
	void OnMouseMove(int x, int y);
	void OnKeyboard(unsigned char key, int x, int y);
	void OnKeyboardUp(unsigned char key, int x, int y);
	void OnReshape(int width, int height);
	void OnVisibility();

	glm::vec2 GetMousePosition() { return mMousePosition;  }
	glm::vec2 GetMousePositionRelative() { return mMousePositionRelative; }

	double GetElapsed() { return  mElapsed; /*mRefreshTime;*/ /*mTimer.GetElapsed();*/ }

	Timer GetTimer() const { return mTimer; }

	//bool IsKeyDown(unsigned char key) { return mKeys[key]; }

	void AddHandler(KeyboardHandler* handler);
	void AddHandler(MouseHandler* handler);

    static inline void CBOnMouse(int button, int state, int x, int y) {
		Application::getInstance().OnMouse(button, state, x, y);
    }
	static inline void CBOnMouseMove(int x, int y) {
		Application::getInstance().OnMouseMove(x, y);
	}

    static inline void CBOnKeyboard(unsigned char key, int x, int y) {
		Application::getInstance().OnKeyboard(key, x, y);
    }

	static inline void CBOnKeyboardUp(unsigned char key, int x, int y) {
		Application::getInstance().OnKeyboardUp(key, x, y);
	}

    static inline void CBOnVisibility(int state) {
        Application::getInstance().OnVisibility();
    }

    static inline void CBOnReshape(int width, int height) {
		Application::getInstance().OnReshape(width, height);
    }
};

#endif	/* APPLICATION_H */

