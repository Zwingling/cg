
#ifndef Light_h__
#define Light_h__

#include "ToonShadedObject.h"
#include "Geometry.h"
#include "LightObject.h"

#include <vector>
#include <string>

class Light : public LightObject
{
protected:
	glm::vec3 mColor;

public:
	Light(glm::vec3 color);
	~Light();

	glm::vec3 GetColor() { return mColor; }

private:
	std::vector<Geometry> LoadGeometry(const char* dir, const char* filename);
	FullSceneObject* CreateSceneObject(Light* parent, std::vector<Geometry> meshes, const char* diffuseTex);
	FullSceneObject* CreateSceneObject(std::vector<Geometry> meshes, const char* diffuseTex);

	virtual void OnAttach();
};


#endif // Carousel_h__
