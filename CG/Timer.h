#ifndef Timer_H 
#define Timer_H

#include <chrono>

typedef std::chrono::high_resolution_clock Clock;
typedef Clock::time_point TimePoint;
typedef std::chrono::milliseconds::rep Milliseconds;

class Timer
{
public:
	Timer();

	void Start();

	void Stop();

	Milliseconds GetElapsedMilliseconds();

	float GetElapsed();

	static Timer StartNew();

private:
	bool m_running;
	TimePoint m_start;
	TimePoint m_end;
};


#endif
