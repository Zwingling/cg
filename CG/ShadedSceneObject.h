#ifndef ShadedSceneObject_h__
#define ShadedSceneObject_h__

#include "SceneObject.h"
#include "RenderComponent.h"

class ShadedSceneObject : public SceneObject
{
public:
	virtual IRenderComponent* GetRenderComponent() = 0;
};

#endif // ShadedSceneObject_h__
