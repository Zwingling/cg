#ifndef SimpleObject_h__
#define SimpleObject_h__

#include "ShaderVariable.h"
#include "Geometry.h"
#include "FullSceneObject.h"
#include "Camera.h"
#include "Application.h"
#include "Scene.h"
#include "KeyboardHandler.h"
#include "PhongShaderComponent.h"

class ToonShadedObject : public FullSceneObject, KeyboardHandler
{ 
private:
	ShaderComponent* mShaderComponent;
	ShaderComponent* mSilhouetteShaderComponent;

	glm::vec3 mOldDiffuse;
	glm::vec3 mOldAmbient;
	glm::vec3 mOldSpecular;

	bool diffuseToggle;
	bool ambientToggle;
	bool specularToggle;
	
	Geometry mGeometry;
	std::string mDiffuseFilename;

public:
	ToonShadedObject();

	void SetGeometry(Geometry geom)
	{
		mGeometry = geom;
		mGeometry.Generate();
	}

	void SetDiffuse(std::string diffuse)
	{
		mDiffuseFilename = diffuse;
	}

private:

	virtual void OnAttach();

	virtual void OnRender();

	virtual void OnUpdate() {}

	virtual void OnKeyUp(unsigned char key);
};

#endif // SimpleObject_h__