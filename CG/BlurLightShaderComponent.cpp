#include "BlurLightShaderComponent.h"
#include "Application.h"

BlurLightShaderComponent::BlurLightShaderComponent(FullSceneObject* owner)
	: ShaderComponent("Blur Shader Component", owner, "resource/shaders/Blur.vertexshader", "resource/shaders/Blur.fragmentshader")
{}

void BlurLightShaderComponent::OnInit()
{
	ShaderComponent::OnInit();

	mTextureSamplerSV = GetShaderVariable("TextureSampler");
	mShift = GetShaderVariable("uShift");
}

void BlurLightShaderComponent::Bind()
{
	ShaderComponent::Bind();

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, Application::getInstance().GetRenderTarget());
	mTextureSamplerSV.SetSampler(0);

	mShift.SetVec2(glm::vec2(1.0 / Application::getInstance().GetWindowWidth(),
							 1.0 / Application::getInstance().GetWindowHeight()));
}
 