#include "CameraPathAnimation.h"
#include "Application.h"
#include "Camera.h"

void CameraPathAnimation::OnInit()
{

}

void CameraPathAnimation::OnUpdate()
{
	if (mRunning)
	{
		mTime += Application::getInstance().GetElapsed();

		while (!mWorkQueue.empty() && mTime > mWorkQueue.front().t)
		{
			mFrom = mWorkQueue.front();
			mWorkQueue.pop();
		}

		SPathPoint to = mFrom;

		if (mWorkQueue.empty())
		{
			if (mLoop)
			{
				Start(true);
			}
			else
			{
				mRunning = false;
			}	
		}
		else
		{
			to = mWorkQueue.front();
		}

		auto owner = GetOwner();
		auto pos = mFrom.pos;
		auto lookAt = mFrom.lookAt;

		if (mFrom != to)
		{
			float t = (mTime - mFrom.t) / (to.t - mFrom.t);

			pos = Interpolate(mFrom.pos, mFrom.posTangent, to.pos, to.posTangent, t);
			lookAt = Interpolate(mFrom.lookAt, mFrom.lookAtTangent, to.lookAt, to.lookAtTangent, t);
		}	

		owner->LookAt(pos, lookAt, owner->GetUp());
	}
}

CameraPathAnimation* CameraPathAnimation::AddPoint(float t, const glm::vec3& pos, const glm::vec3& posTangent, const glm::vec3& lookAt, const glm::vec3& lookAtTangent)
{
	SPathPoint p;
	p.pos = pos;
	p.lookAt = lookAt;
	p.t = t;
	p.posTangent = posTangent;
	p.lookAtTangent = lookAtTangent;

	mPath.push(p);

	printf("AddPoint(%.2f, glm::vec3(%.2f, %.2f, %.2f), glm::vec3(%.2f, %.2f, %.2f), glm::vec3(%.2f, %.2f, %.2f), glm::vec3(%.2f, %.2f, %.2f))\n",
		   t,
		   p.pos.x,
		   p.pos.y,
		   p.pos.z,
		   p.posTangent.x,
		   p.posTangent.y,
		   p.posTangent.z,
		   p.lookAt.x,
		   p.lookAt.y,
		   p.lookAt.z,
		   p.lookAtTangent.x,
		   p.lookAtTangent.y,
		   p.lookAtTangent.z);

	return this;
}

CameraPathAnimation* CameraPathAnimation::AppendPoint(float dt, const glm::vec3& pos, const glm::vec3& lookAt)
{
	glm::vec3 posTangent(0, 0, 0);
	glm::vec3 lookAtTangent(0, 0, 0);
	float t = 0;

	if (!mPath.empty())
	{
		auto pre = mPath.back();

		posTangent = pos - pre.pos;
		lookAtTangent = lookAt - pre.lookAt;
		t = pre.t + dt;
	}

	return AddPoint(t, pos, posTangent, lookAt, lookAtTangent);
}

void CameraPathAnimation::Clear()
{
	Stop();
	mPath = std::queue<SPathPoint>();
}

void CameraPathAnimation::Start(bool loop)
{
	if (mPath.size() >= 2)
	{
		mRunning = true;
		mTime = 0;
		mLoop = loop;

		mWorkQueue = std::queue<SPathPoint>(mPath);

		mFrom = mWorkQueue.front();
		auto owner = GetOwner();

		owner->LookAt(mFrom.pos, mFrom.lookAt, owner->GetUp());
	}
}

void CameraPathAnimation::Stop()
{
	mRunning = false;
	mWorkQueue = std::queue<SPathPoint>();
}

void CameraPathAnimation::Pause()
{
	mRunning = false;
}

void CameraPathAnimation::Resume()
{
	if (!mPath.empty())
		mRunning = true;
}


