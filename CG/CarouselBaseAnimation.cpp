#include "CarouselBaseAnimation.h"
#include "FullSceneObject.h"
#include "Application.h"

CarouselBaseAnimation::CarouselBaseAnimation(FullSceneObject* owner) : GenericComponent<FullSceneObject>("AmimationComponent", owner)
{
}

void CarouselBaseAnimation::OnUpdate()
{
	auto transform = mOwner->GetLocalTransformComponent();
	transform->Rotate(0, mSpeed * Application::getInstance().GetElapsed(), 0);
}
