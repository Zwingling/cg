#include "SilhouetteShaderComponent.h"
#include "Application.h"
#include "Camera.h"
#include "Scene.h"
#include "Light.h"

SilhouetteShaderComponent::SilhouetteShaderComponent(FullSceneObject* owner)
	: ShaderComponent("Silhouette Shader Component", owner, "resource/shaders/Silhouette.vertexshader", "resource/shaders/Silhouette.fragmentshader")
{
}

void SilhouetteShaderComponent::OnInit()
{
	ShaderComponent::OnInit();

	mColorSV = GetShaderVariable("MeshColor");
	mMVPSV = GetShaderVariable("MVP"); 
} 

void SilhouetteShaderComponent::Bind()
{
	auto material = GetOwner()->GetMaterialComponent();
	auto scene = Application::getInstance().GetScene();
	auto pCamera = scene->GetActiveCamera();

	glm::mat4 mv = pCamera->GetView() *  GetOwner()->GetGlobalTransform();
	glm::mat4 mvp = pCamera->GetProjection() * mv;

	ShaderComponent::Bind();

	mMVPSV.SetMatrix(&mvp[0][0]);
	mColorSV.SetVec3(glm::vec3(0, 0, 0));

	glFrontFace(GL_CW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glLineWidth(3);
	glEnable(GL_LINE_SMOOTH);
	//glDepthMask(false);
	//glDepthFunc(GL_ALWAYS);
}

void SilhouetteShaderComponent::Unbind()
{
	glFrontFace(GL_CCW);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glLineWidth(1);
	//glDepthMask(true);
	//glDepthFunc(GL_LESS);
}
