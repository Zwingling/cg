#ifndef STATICSCENEOBJECT_H
#define STATICSCENEOBJECT_H

#include "PhongShadedObject.h"
#include "Geometry.h"

#include <vector>
#include <string>

class StaticSceneObject : public PhongShadedObject
{
public:
	StaticSceneObject();
	~StaticSceneObject();

private:
	std::vector<Geometry> LoadGeometry(const char* dir, const char* filename);
	FullSceneObject* CreateSceneObject(FullSceneObject* parent, std::vector<Geometry> meshes, const char* diffuseTex);
	FullSceneObject* CreateSceneObject(std::vector<Geometry> meshes, const char* diffuseTex);

	virtual void OnAttach();
};

#endif // STATICSCENEOBJECT_H
