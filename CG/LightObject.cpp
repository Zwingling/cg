#include "LightObject.h"
#include "Light.h"
#include <vector>
#include "BlurLightShaderComponent.h"

LightObject::LightObject()
{
	mLightShaderComponent = AddComponent<LightShaderComponent>(this);
	mBlurShaderComponent = AddComponent<BlurLightShaderComponent>(this);

	mRectangle.AddVertex(glm::vec3(-1, -1, 0));
	mRectangle.AddNormal(glm::vec3(0, 0, -1));
	mRectangle.AddTexturCoord(glm::vec2(0, 0));

	mRectangle.AddVertex(glm::vec3(1, -1, 0));
	mRectangle.AddNormal(glm::vec3(0, 0, -1));
	mRectangle.AddTexturCoord(glm::vec2(1, 0)); 
	 
	mRectangle.AddVertex(glm::vec3(-1, 1, 0));
	mRectangle.AddNormal(glm::vec3(0, 0, -1)); 
	mRectangle.AddTexturCoord(glm::vec2(0, 1));  

	mRectangle.AddVertex(glm::vec3(1, 1, 0));
	mRectangle.AddNormal(glm::vec3(0, 0, -1));
	mRectangle.AddTexturCoord(glm::vec2(1, 1));

	mRectangle.AddIndex(0);
	mRectangle.AddIndex(1);
	mRectangle.AddIndex(2);

	mRectangle.AddIndex(2);
	mRectangle.AddIndex(1);
	mRectangle.AddIndex(3);

	mRectangle.Generate();
}


void LightObject::OnRender()
{
	mLightShaderComponent->Bind();
	mGeometry.Render();
	mLightShaderComponent->Unbind();
} 


void LightObject::OnPostRender()
{
	mBlurShaderComponent->Bind();
	mRectangle.Render();
	mBlurShaderComponent->Unbind();
}


void LightObject::OnAttach()
{
	if (mDiffuseFilename != "")
	{
		GetMaterialComponent()->SetDiffuseTexture(mDiffuseFilename.c_str());
	}
	else
	{
		GetMaterialComponent()->SetDiffuseTexture("resource/models/uvmap.DDS");
	}

	this->ambientToggle = true; 
	this->diffuseToggle = true;
	this->specularToggle = true;

	mOldAmbient = GetMaterialComponent()->GetAmbient();
	mOldDiffuse = GetMaterialComponent()->GetDiffuse();
	mOldSpecular = GetMaterialComponent()->GetSpecular();
}

void LightObject::OnKeyUp(unsigned char key)
{
	KeyboardHandler::OnKeyUp(key);

	switch (key)
	{
	case 'u':
	{
		if (diffuseToggle)
		{
			mOldDiffuse = this->GetMaterialComponent()->GetDiffuse();
			this->GetMaterialComponent()->SetDiffuse(glm::vec3(0.0, 0.0, 0.0));
		}
		else
		{
			this->GetMaterialComponent()->SetDiffuse(mOldDiffuse);
		}
		diffuseToggle = !diffuseToggle;
		break; 
	}
	case 'i':
	{
		if (ambientToggle)
		{
			mOldAmbient = this->GetMaterialComponent()->GetAmbient();
			this->GetMaterialComponent()->SetAmbient(glm::vec3(0.0, 0.0, 0.0));
		}
		else
		{
			this->GetMaterialComponent()->SetAmbient(mOldAmbient);
		} 
		ambientToggle = !ambientToggle;
		break;
	}
	case 'o':
	{
		if (specularToggle)
		{
			mOldSpecular = this->GetMaterialComponent()->GetSpecular();
			this->GetMaterialComponent()->SetSpecular(glm::vec3(0.0, 0.0, 0.0));
		}
		else
		{
			this->GetMaterialComponent()->SetSpecular(mOldSpecular);
		}
		specularToggle = !specularToggle;
		break;
	}
	}
}

