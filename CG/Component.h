#ifndef Component_H
#define Component_H

#include <string>

class Component
{
public:
	Component(const char* name) : mName(name)
	{
	}

	const char* GetName() { return mName.c_str(); }
	virtual void OnInit(){}
	virtual void OnUpdate(){}

private:
	std::string mName;
};

template<class OwnerType>
class GenericComponent : public Component
{
protected:
	OwnerType* mOwner;

public:
	GenericComponent(const char* name, OwnerType* owner) : Component(name)
	{
		mOwner = owner;
	}
	
	OwnerType* GetOwner() { return mOwner; };
};

#endif
