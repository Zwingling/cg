#include "Transform.h"
#include "glm/gtc/matrix_transform.hpp"

Transform::Transform()
{
	mTransform = glm::mat4(1.0f);
	mScale = glm::vec3(1, 1, 1);
}

void Transform::Rotate(glm::vec3 v)
{
	mRotation += v;
	Update();
}

void Transform::Translate(glm::vec3 v)
{
	mTranslation += v;
	Update();
}

void Transform::Scale(glm::vec3 v)
{
	mScale += v;
	Update();
}

void Transform::Rotate(float x, float y, float z)
{
	mRotation.x += x;
	mRotation.y += y;
	mRotation.z += z;
	Update();
}

void Transform::Translate(float x, float y, float z)
{
	mTranslation.x += x;
	mTranslation.y += y;
	mTranslation.z += z;
	Update();
}

void Transform::Scale(float x, float y, float z)
{
	mScale.x += x;
	mScale.y += y;
	mScale.z += z;
	Update();
}

void Transform::Update()
{
 	mTransform = glm::scale(glm::mat4(1.0f), mScale);
 	mTransform = glm::rotate(mTransform, mRotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
 	mTransform = glm::rotate(mTransform, mRotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
 	mTransform = glm::rotate(mTransform, mRotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
	mTransform = glm::translate(mTransform, mTranslation);
}

void Transform::SetRotation(glm::vec3 v)
{
	mRotation = v; 
	Update();
}

void Transform::SetTranslation(glm::vec3 v)
{
	mTranslation = v; 
	Update();
}

void Transform::SetScale(glm::vec3 v)
{
	mScale = v; 
	Update();
}
