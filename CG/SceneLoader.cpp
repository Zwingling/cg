#include "SceneLoader.h"

#include "tinyobj.h"
#include <iostream>

std::vector<Mesh> SceneLoader::Load(const char* dir, const char* filename)
{
	std::vector<Mesh> meshes;

	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string err = tinyobj::LoadObj(shapes, materials, (std::string(dir) + filename).c_str(), dir);

	if (!err.empty()) 
	{
		std::cerr << err << std::endl;
		meshes;
	}

	float maxVertex = 0.0f;

	for (size_t i = 0; i < shapes.size(); i++)
	{
		Mesh mesh;
		mesh.name = shapes[i].name;
		mesh.indices = shapes[i].mesh.indices;

		for (size_t v = 0; v < shapes[i].mesh.positions.size() / 3; v++)
		{
			int index = 3 * v;

			glm::vec3 vertex = glm::vec3(shapes[i].mesh.positions[index + 0], shapes[i].mesh.positions[index + 1], shapes[i].mesh.positions[index + 2]);
			mesh.vertices.push_back(vertex);

			if (!shapes[i].mesh.normals.empty())
				mesh.normals.push_back(glm::vec3(shapes[i].mesh.normals[index + 0], shapes[i].mesh.normals[index + 1], shapes[i].mesh.normals[index + 2]));

			index = 2 * v;
			if (!shapes[i].mesh.texcoords.empty())
				mesh.texCoords.push_back(glm::vec2(shapes[i].mesh.texcoords[index + 0], shapes[i].mesh.texcoords[index + 1]));
		}
		meshes.push_back(mesh);


		for (size_t i = 0; i < materials.size(); i++) 
		{
			printf("material[%ld].name = %s\n", i, materials[i].name.c_str());
			printf("  material.Ka = (%f, %f ,%f)\n", materials[i].ambient[0], materials[i].ambient[1], materials[i].ambient[2]);
			printf("  material.Kd = (%f, %f ,%f)\n", materials[i].diffuse[0], materials[i].diffuse[1], materials[i].diffuse[2]);
			printf("  material.Ks = (%f, %f ,%f)\n", materials[i].specular[0], materials[i].specular[1], materials[i].specular[2]);
			printf("  material.Tr = (%f, %f ,%f)\n", materials[i].transmittance[0], materials[i].transmittance[1], materials[i].transmittance[2]);
			printf("  material.Ke = (%f, %f ,%f)\n", materials[i].emission[0], materials[i].emission[1], materials[i].emission[2]);
			printf("  material.map_Ka = %s\n", materials[i].ambient_texname.c_str());
			printf("  material.map_Kd = %s\n", materials[i].diffuse_texname.c_str());
			printf("  material.map_Ks = %s\n", materials[i].specular_texname.c_str());
			printf("  material.map_Ns = %s\n", materials[i].normal_texname.c_str());
		}
	}


	return meshes;
}

