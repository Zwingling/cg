#ifndef BillboardObject_h__
#define BillboardObject_h__

#include "ShaderVariable.h"
#include "Geometry.h"
#include "FullSceneObject.h"
#include "Camera.h"
#include "Application.h"
#include "Scene.h"
#include "KeyboardHandler.h"
#include "ShaderComponent.h"

class BillboardObject : public FullSceneObject, KeyboardHandler
{
public:
	glm::vec3 mColor;

private:
	ShaderComponent* mShaderComponent;

	Geometry mGeometry;

public:
	BillboardObject();

	void SetGeometry(Geometry geom)
	{
		mGeometry = geom;
		mGeometry.Generate();
	}

	virtual void OnAttach();

	virtual void OnRender();

	virtual void OnUpdate() {}
};

#endif