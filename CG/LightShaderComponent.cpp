#include "LightShaderComponent.h"
#include "Application.h"
#include "Camera.h" 
#include "Scene.h" 
#include "Light.h"

LightShaderComponent::LightShaderComponent(FullSceneObject* owner)
	: ShaderComponent("Light Shader Component", owner, "resource/shaders/Light.vertexshader", "resource/shaders/Light.fragmentshader")
{} 

void LightShaderComponent::OnInit()
{ 
	ShaderComponent::OnInit();

	mTextureSamplerDiffuseSV = GetShaderVariable("DiffuseTextureSampler");
	mTextureSamplerSpecularSV = GetShaderVariable("SpecularTextureSampler");
	mTextureSamplerNormalSV = GetShaderVariable("NormalTextureSampler");
	mColorSV = GetShaderVariable("MeshColor");
	mAmbientSV = GetShaderVariable("ambientColor");
	mSpecularSV = GetShaderVariable("specularColor");
	mDiffuseSV = GetShaderVariable("diffuseColor");
	mMVPSV = GetShaderVariable("MVP");
	mViewSV = GetShaderVariable("V");
	mModelSV = GetShaderVariable("M");
	mModel3x3SV = GetShaderVariable("MV3x3");
	mLightPosition = GetShaderVariable("LightPosition_worldspace");
	mNumberOfLights = GetShaderVariable("numberOfLights");
	mLightColor = GetShaderVariable("asdf");

	for (long long int i = 0; i < MaxNumberOfLights; i++)
	{
		mLights[i].position = GetShaderVariable(("lights[" + std::to_string(i) + "].position").c_str());
		mLights[i].color = GetShaderVariable(std::string("lights[" + std::to_string(i) + "].color").c_str());
	}
}

void LightShaderComponent::Bind()
{
	auto material = GetOwner()->GetMaterialComponent();
	auto scene = Application::getInstance().GetScene();
	auto pCamera = scene->GetActiveCamera();

	glm::mat4 mv = pCamera->GetView() *  GetOwner()->GetGlobalTransform();
	glm::mat4 mvp = pCamera->GetProjection() * mv;
	glm::mat4 view = pCamera->GetView();
	glm::mat4 model = GetOwner()->GetGlobalTransform();
	glm::mat3 mv3x3 = glm::mat3(mv);

	auto lights = scene->GetLights();
	std::vector<glm::vec3> lightsPos;
	int numberOfLights = lights.size() > MaxNumberOfLights ? MaxNumberOfLights : lights.size();

	ShaderComponent::Bind();

	mMVPSV.SetMatrix(&mvp[0][0]);
	mViewSV.SetMatrix(&view[0][0]);
	mModelSV.SetMatrix(&model[0][0]);
	mModel3x3SV.SetMatrix3(&mv3x3[0][0]);

	mNumberOfLights.SetInteger(numberOfLights);
	for (int i = 0; i < numberOfLights; i++)
	{
		auto position = glm::vec3(lights[i]->GetGlobalTransform()[3]);
		auto color = glm::vec3(lights[i]->GetColor());

		mLights[i].position.SetVec3(position);
		mLights[i].color.SetVec3(color);
	}

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, material->GetDiffuseTexture());
	mTextureSamplerDiffuseSV.SetSampler(0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, material->GetNormalTexture());
	mTextureSamplerNormalSV.SetSampler(1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, material->GetSpecularTexture());
	mTextureSamplerSpecularSV.SetSampler(2);

	mColorSV.SetVec3(material->GetColor());
	mDiffuseSV.SetVec3(material->GetDiffuse());
	mAmbientSV.SetVec3(material->GetAmbient());
	mSpecularSV.SetVec3(material->GetSpecular());

	Light* light = dynamic_cast<Light*>(GetOwner()->GetParent());
	if (light != NULL)
		mLightColor.SetVec3(light->GetColor());
}
