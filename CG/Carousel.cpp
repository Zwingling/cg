  #include "Carousel.h"
#include "Geometry.h"
#include "SceneLoader.h"
#include "Camera.h"
#include "Application.h"
#include "Scene.h"
#include "CarouselFishAnimation.h"
#include "CarouselBaseAnimation.h"
#include "ToonShadedObject.h"

#include "AnimatedLight.h"
#include "LightVAnimation.h"

#include "glm/gtc/constants.hpp"


Carousel::Carousel()
{}


Carousel::~Carousel()
{}

FullSceneObject* Carousel::CreateSceneObject(std::vector<Geometry> geometry, const char* diffuseTex)
{
	return CreateSceneObject(new FullSceneObject(), geometry, diffuseTex);
}

FullSceneObject* Carousel::CreateSceneObject(FullSceneObject* parent, std::vector<Geometry> geometry, const char* diffuseTex)
{
	for (auto it = geometry.begin(); it != geometry.end(); it++)
	{
		auto pChild = new ToonShadedObject();
		pChild->SetGeometry(*it);
		pChild->SetDiffuse(std::string("resource/models/") + diffuseTex);
		parent->AddChild(pChild);
	}

	return parent;
}

void Carousel::OnAttach()
{
	const float radius = 3.4f;
	const float fishScale = 0.7f;
	const float fishOriginHeight = 3.0f;
	const float fishSpeed = 1.3f;
	const float fishRange = 2.0f;
	const float baseScale = 1.0f; 
	const float baseRotationSpeed = -2.0f;

	auto baseGeometry = LoadGeometry("resource/models/", "base3.obj");
	auto fishGeometry = LoadGeometry("resource/models/", "wraisse_main.obj");
	auto lightGeometry = LoadGeometry("resource/models/", "sphere.obj");

	CreateSceneObject(this, baseGeometry, "base_diffuse.bmp");
	this->GetLocalTransformComponent()->SetScale(glm::vec3(baseScale, baseScale, baseScale));
	this->AddComponent<CarouselBaseAnimation>(this)->SetSpeed(baseRotationSpeed);
	this->GetMaterialComponent()->SetColor(glm::vec3(1.0f, 0.1f, 0.1f));


	auto fish = CreateSceneObject(fishGeometry, "uvmap.DDS");
	fish->GetLocalTransformComponent()->SetScale(glm::vec3(fishScale, fishScale, fishScale));
	fish->GetLocalTransformComponent()->SetRotate(glm::vec3(0.0f, -glm::pi<float>() / 2.0f, 0.0f));
	fish->GetLocalTransformComponent()->SetTranslate(glm::vec3(radius, fishOriginHeight, 0.0f));
	fish->AddComponent<CarouselFishAnimation>(fish)
		->SetRange(fishRange)
		->SetSpeed(fishRange)
		->SetOffset(0);

	this->AddChild(fish);

	fish = CreateSceneObject(fishGeometry, "uvmap.DDS");
	fish->GetLocalTransformComponent()->SetScale(glm::vec3(fishScale, fishScale, fishScale));
	fish->GetLocalTransformComponent()->SetRotate(glm::vec3(0.0f, glm::pi<float>() / 2.0f, 0.0f));
	fish->GetLocalTransformComponent()->SetTranslate(glm::vec3(-radius, fishOriginHeight, 0.0f));
	fish->AddComponent<CarouselFishAnimation>(fish)
		->SetRange(fishRange)
		->SetSpeed(fishRange)
		->SetOffset(glm::pi<float>() / 2.0f);

	this->AddChild(fish);

	fish = CreateSceneObject(fishGeometry, "uvmap.DDS");
	fish->GetLocalTransformComponent()->SetScale(glm::vec3(fishScale, fishScale, fishScale));
	fish->GetLocalTransformComponent()->SetRotate(glm::vec3(0.0f, 2*glm::pi<float>() / 2.0f, 0.0f));
	fish->GetLocalTransformComponent()->SetTranslate(glm::vec3(0.0f, fishOriginHeight, radius));
	fish->AddComponent<CarouselFishAnimation>(fish)
		->SetRange(fishRange)
		->SetSpeed(fishRange)
		->SetOffset(glm::pi<float>());

	this->AddChild(fish);

	fish = CreateSceneObject(fishGeometry, "uvmap.DDS");
	fish->GetLocalTransformComponent()->SetScale(glm::vec3(fishScale, fishScale, fishScale));
	fish->GetLocalTransformComponent()->SetRotate(glm::vec3(0.0f, 0, 0.0f));
	fish->GetLocalTransformComponent()->SetTranslate(glm::vec3(0.0f, fishOriginHeight, -radius));
	fish->AddComponent<CarouselFishAnimation>(fish)
		->SetRange(fishRange)
		->SetSpeed(fishRange)
		->SetOffset(3 * glm::pi<float>() / 2.0f);

	this->AddChild(fish);


	auto animLight = new AnimatedLight(glm::vec3(3.0f, 2.0f, 0.0f));
	animLight->GetLocalTransformComponent()->SetTranslate(glm::vec3(-7, 13, -15));

	const float lightSpeed = 1.0f;
	const float lightRange = 8.0f;
	animLight->AddComponent<LightVAnimation>(animLight)
		->SetRange(lightRange)
		->SetSpeed(lightSpeed)
		->SetOffset(0);

	Application::getInstance().GetScene()->AddLight(animLight);
	this->AddChild(animLight);
}

std::vector<Geometry> Carousel::LoadGeometry(const char* dir, const char* filename)
{
	auto meshes = SceneLoader::getInstance().Load(dir, filename);
	std::vector<Geometry> geometry;

	for (auto it = meshes.begin(); it != meshes.end(); it++)
	{
		Geometry geom;

		geom.AddIndices((*it).indices.data(), (*it).indices.size());

		for (unsigned int i = 0; i < (*it).vertices.size(); i++)
		{
			geom.AddVertex((*it).vertices[i]);

			if (!(*it).normals.empty())
				geom.AddNormal((*it).normals[i]);

			if (!(*it).texCoords.empty())
				geom.AddTexturCoord((*it).texCoords[i]);
		}

		geometry.push_back(geom);
	}

	return geometry;
}
