#ifndef RenderComponent_h__
#define RenderComponent_h__

class IRenderComponent
{
public:
	virtual void Render() = 0;

	virtual class MaterialComponent* GetMaterialComponent() = 0;
	virtual class ShaderComponent* GetShaderComponent() = 0;
	virtual void SetGeometry(class Geometry&) = 0;
};

#endif // RenderComponent_h__
