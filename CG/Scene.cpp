#include "Scene.h"
#include "SceneObject.h"
#include "Camera.h"
#include "Light.h"

Scene::Scene()
{
	mCurrentID = 0; 
}

void Scene::OnRender()
{      
    std::map<unsigned int, SceneObject*>::iterator itr = mObjects.begin();
    for(; itr != mObjects.end(); itr++)
        itr->second->CBOnRender();
}

void Scene::OnUpdate()
{
    std::map<unsigned int, SceneObject*>::iterator itr = mObjects.begin();
    for(; itr != mObjects.end(); itr++)
		itr->second->CBOnUpdate();
}

void Scene::AddObject(SceneObject* object)
{
    mObjects[object->GetId()] = object;
	object->CBOnAttach(this);
}

unsigned int Scene::GetNewId()
{
   return mCurrentID++; 
}

bool Scene::SetActiveCamera(const char* name)
{
	if (mActiveCamera) mActiveCamera->Disable();

	auto itr = mCameras.find(name);
	if (itr == mCameras.end())
		return false;

	mActiveCamera = itr->second;
	mActiveCamera->Enable();

	return true;
}

Camera* Scene::AddCamera(Camera* camera)
{
	AddObject(camera);
	mCameras[camera->GetName()] = camera;
	camera->Disable();
	return camera;
}

Light* Scene::AddLight(Light* light)
{
	mLights.push_back(light);
	return light;
}

void Scene::OnPostRender()
{
	std::map<unsigned int, SceneObject*>::iterator itr = mObjects.begin();
	for (; itr != mObjects.end(); itr++)
		itr->second->CBOnPostRender();
}
