#include "CarouselFishAnimation.h"
#include "FullSceneObject.h"
#include "Application.h"

CarouselFishAnimation::CarouselFishAnimation(FullSceneObject* owner) : GenericComponent<FullSceneObject>("AmimationComponent", owner)
{
	mOldTranslation = owner->GetLocalTransformComponent()->GetTranslation();
}

void CarouselFishAnimation::OnUpdate()
{
	auto transform = mOwner->GetLocalTransformComponent();

	mTime += (float)Application::getInstance().GetElapsed();

	auto v = mRange * glm::sin(mTime * mSpeed + mOffset);

	transform->SetTranslate(glm::vec3(mOldTranslation.x, mOldTranslation.y + v, mOldTranslation.z));
}
