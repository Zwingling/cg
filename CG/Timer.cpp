#include "Timer.h"


Timer::Timer()
{
	m_start = TimePoint::min();
	m_end = TimePoint::min();
	m_running = false;
}

Timer Timer::StartNew()
{
	Timer timer;
	timer.Start();
	return timer;
}

float Timer::GetElapsed()
{
	return GetElapsedMilliseconds() / 1000.0f;
}

Milliseconds Timer::GetElapsedMilliseconds()
{
	TimePoint end = m_running ? Clock::now() : m_end;
	return std::chrono::duration_cast<std::chrono::milliseconds>(end - m_start).count();
}

void Timer::Stop()
{
	m_end = Clock::now();
	m_running = false;
}

void Timer::Start()
{
	m_start = Clock::now();
	m_running = true;
}
