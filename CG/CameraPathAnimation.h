#ifndef CameraPathAnimation_h__
#define CameraPathAnimation_h__

#include "Component.h"
#include <queue>
#include "glm/common.hpp"

class Camera;

class CameraPathAnimation : public GenericComponent<Camera>
{
public:
	CameraPathAnimation(Camera* owner) : GenericComponent<Camera>("CameraPathAnimation", owner) {};

	CameraPathAnimation* AddPoint(float t, 
								  const glm::vec3& pos, const glm::vec3& posTangent, 
								  const glm::vec3& lookAt, const glm::vec3& lookAtTangent);

	CameraPathAnimation* AppendPoint(float dt,
									 const glm::vec3& pos,
									 const glm::vec3& lookAt);

	void Clear();

	void Start(bool loop = false);
	void Stop();
	void Pause();
	void Resume();

	bool IsRunning() const { return mRunning; }

	virtual void OnInit();
	virtual void OnUpdate();

private:	
	struct SPathPoint
	{
		float t;
		glm::vec3 pos;
		glm::vec3 lookAt;
		glm::vec3 posTangent;
		glm::vec3 lookAtTangent;

		bool operator == (const SPathPoint& rhs)
		{
			return (t == rhs.t && 
					pos == rhs.pos && 
					lookAt == rhs.lookAt && 
					posTangent == rhs.posTangent &&
					lookAtTangent == rhs.lookAtTangent);
		}

		bool operator != (const SPathPoint& rhs)
		{
			return !(*this == rhs);
		}
	};

	double mTime;
	bool mRunning;
	bool mLoop;

	std::queue<SPathPoint> mPath;	
	std::queue<SPathPoint> mWorkQueue;
	SPathPoint mFrom;

	template<typename T>
	T Interpolate(const T& value1, const T& tangent1, const T& value2, const T& tangent2, const float t)
	{
		const T c0(2.0f * value1 - 2.0f * value2 + tangent1 + tangent2);
		const T c1(3.0f * value2 - 3.0f * value1 - 2.0f * tangent1 - tangent2);

		return c0 * (t * t * t) + c1 * (t * t) + tangent1 * t + value1;
	}

	template<typename T>
	T InterpolateVelocity(const T& value1, const T& tangent1, const T& value2, const T& tangent2, const float t)
	{
		const T c0(2.0f * value1 - 2.0f * value2 + tangent1 + tangent2);
		const T c1(3.0f * value2 - 3.0f * value1 - 2.0f * tangent1 - tangent2);

		return 3.0f * c0 * (t * t) + 2.0f * c1 * t + tangent1;
	}
};


#endif // CameraPathAnimation_h__

