#ifndef RotationCamera_h_included
#define RotationCamera_h_included

#include "Camera.h"
#include "KeyboardHandler.h"
#include "Application.h"
#include "glm/gtx/perpendicular.hpp"
#include "Timer.h"

class RotationCamera : public Camera, KeyboardHandler
{
private:
	glm::vec3 mPosition;
	glm::vec3 mNextCamPos;
	glm::vec3 mDirection;
	glm::vec2 mStrafe;
	glm::vec3 mOrig;
	float mSpeed;
	float mCamAngle;
	float mRotationSpeed;
	float mYaw;
	float mPitch;
	bool clockwise;
	bool mPaused;
	bool mRotateAxixX;
	Timer *timer;

public:
	RotationCamera(const char* name) : Camera(name)
	{
		mSpeed = 10.0f;
		mCamAngle = 0.0f;
		mRotationSpeed = 0.01f;
		clockwise = true;
		mPaused = false;
		timer = new Timer();
		timer->Start();
		mOrig = glm::vec3(0);
		mRotateAxixX = true;
		mDirection = glm::vec3(0, 0, 1);
	}

	virtual void OnAttach()
	{

	}

	virtual void OnRender()
	{

	}

	virtual void OnUpdate()
	{
		auto t = (float)Application::getInstance().GetElapsed();
		auto speed = mSpeed*t;

		if (std::string("rotation") == Application::getInstance().GetScene()->GetActiveCamera()->GetName())
		{
			if (IsKeyDown('w'))
			{
				Move(speed);
			}
			else if (IsKeyDown('s'))
			{
				Move(-speed);
			}
			else if (IsKeyDown('a'))
			{
				mPosition.x += speed;
				Rebuild();
			}
			else if (IsKeyDown('d'))
			{
				mPosition.x -= speed;
				Rebuild();
			}
			else if (IsKeyDown('q'))
			{
				mPosition.y += speed;
				Rebuild();
			}
			else if (IsKeyDown('e'))
			{
				mPosition.y -= speed;
				Rebuild();
			}
			// speed up
			else if (IsKeyDown('r')){
				mRotationSpeed += 0.005f;
				Rebuild();
			}
			// slow down
			else if (IsKeyDown('t')){
				// avoid direction switch
				if (mRotationSpeed > 0.009){
					mRotationSpeed -= 0.005f;
					Rebuild();
				}
				
			}
			// pause
			else if (IsKeyDown('p')){
				// half second between toggle to avoid double toggle
				if (timer->GetElapsed() > 0.5f){
					mPaused = !mPaused;
					timer->Start();
				}
			}
			// switch direction of rotation
			else if (IsKeyDown('g')){
				// half second between toggle to avoid double toggle
				if (timer->GetElapsed() > 0.5f){
					clockwise = !clockwise;
					timer->Start();
					Rebuild();
				}
			}
			// switch axis of rotation
			else if (IsKeyDown('z')){
				// half second between toggle to avoid double toggle
				if (timer->GetElapsed() > 0.5f){
					mRotateAxixX = !mRotateAxixX;
					timer->Start();
					Rebuild();
				}
			}
			// reset to start
			else if (IsKeyDown('m')){
				SetPerspective(45.0f, Application::getInstance().GetAspectRatio(), 0.1f, 1000.0f);
				SetPosition(glm::vec3(0, 5, -30));
				mRotationSpeed = 0.01f;
				mRotateAxixX = true;
				clockwise = true;
				mCamAngle = 0.0f;
				Rebuild();
			}
			else {
				Rebuild();
			}
		}

	}

	void SetPosition(glm::vec3 position)
	{
		mPosition = position;
		Rebuild();
	}

	void Rebuild()
	{
		if (!mPaused){
			if (clockwise){
				mCamAngle += mRotationSpeed;
			}
			else {
				mCamAngle -= mRotationSpeed;
			}
			// pitch
			if (mRotateAxixX){
				// z value of position is distance to origin
				mNextCamPos.x = glm::cos(mCamAngle)*mPosition.z;
				mNextCamPos.y = mPosition.y;
			}
			// yaw
			else {
				// z value of position is distance to origin
				mNextCamPos.x = mPosition.x;
				mNextCamPos.y = glm::cos(mCamAngle)*mPosition.z;
			}
			mNextCamPos.z = glm::sin(mCamAngle)*mPosition.z;
			glm::vec3 mLookAtVec;
			
			mLookAtVec = mOrig - mNextCamPos;
			LookAt(mNextCamPos, mLookAtVec, glm::vec3(0, 1, 0));
		}
	}

	void Move(float incr)
	{
		mPosition += (mDirection*incr);
		Rebuild();
	}

	virtual void OnLBUp()
	{

	}

	virtual void OnLBDown()
	{}

};

#endif