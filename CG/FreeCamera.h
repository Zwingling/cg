#ifndef FreeCamera_h_included
#define FreeCamera_h_included

#include "Camera.h"
#include "KeyboardHandler.h"
#include "MouseHandler.h"
#include "Application.h"
#include "glm/gtx/perpendicular.hpp"
#include "glm/gtx/vector_angle.hpp"

class FreeCamera : public Camera, KeyboardHandler, MouseHandler
{
private:
	float mYaw;
	float mTurnVel;

	float mPitch;
	float mLookVel;
	float maxPitch;

	float mMoveDir;
	float mMoveVel;
	float mMoveMaxVel;
	float mMoveDec;
	float mMoveAcc;

	float mStrafeDir;
	float mStrafeVel;
	float mStrafeMaxVel;
	float mStrafeAcc;
	float mStrafeDec;

	float mMouseChangeX;
	float mMouseChangeY;

	float clamp(float val, float min, float max)
	{
		if (val > max)		return max;
		else if (val < min) return min;
		else				return val;
	}

public:
	FreeCamera(const char* name) : Camera(name)
	{
		mMoveDir = 0;
		mMoveVel = 0;
		mMoveMaxVel = 50.0f;
		mMoveDec = 0.5f;
		mMoveAcc = 500;

		mStrafeDir = 0;
		mStrafeVel = 0;
		mStrafeMaxVel = 50.0f;
		mStrafeAcc = 500;
		mStrafeDec = 0.5f; 

		mTurnVel = 0.1f;
		mLookVel = 0.1f;
		maxPitch = glm::radians<float>(85);

		mYaw = 0;
		mPitch = 0;

		mMouseChangeX = 0;
		mMouseChangeY = 0;

		LookAt(glm::vec3(0, 10, -40), glm::vec3(0, 5, 0), glm::vec3(0, 1, 0));
	}

	virtual void OnAttach()
	{

	}

	virtual void OnRender()
	{

	}

	virtual void OnUpdate()
	{
		auto t = (float)Application::getInstance().GetElapsed();

		// update vels
		mMoveVel += mMoveDir * mMoveAcc * t;
		mStrafeVel += mStrafeDir * mStrafeAcc * t;

		mMoveVel = clamp(mMoveVel, -mMoveMaxVel, mMoveMaxVel);
		mStrafeVel = clamp(mStrafeVel, -mStrafeMaxVel, mStrafeMaxVel);

		// get vecs
		auto eyePos = GetEyePos();
		auto upVec = GetUp();
		auto lookDir2 = GetLookAt()  - eyePos;

		mYaw += mMouseChangeX * t * mTurnVel;
		mPitch += mMouseChangeY * t * mLookVel;

		mPitch = clamp(mPitch, -maxPitch, maxPitch);
		
		if (glm::abs(mYaw) > glm::two_pi<float>())
		{
			mYaw -= glm::sign(mYaw) * glm::two_pi<float>();
		}

		glm::vec3 lookDir;
		lookDir.x = glm::sin(mYaw) * glm::cos(mPitch);
		lookDir.y = glm::sin(mPitch);
		lookDir.z = glm::cos(mYaw) * glm::cos(mPitch);

		auto right = glm::normalize(glm::cross(upVec, lookDir));

		lookDir2 = glm::rotate(lookDir2, -mMouseChangeY * t * mLookVel, right);
		lookDir2 = glm::rotate(lookDir2, mMouseChangeX * t * mTurnVel, upVec);

		mMouseChangeY = 0;
		mMouseChangeX = 0;

		// translation
		auto moveVec = glm::normalize(lookDir) * mMoveVel * t;
		auto strafeVec = right * mStrafeVel * t;
		auto transVec = moveVec + strafeVec;

		eyePos += transVec;

		LookAt(eyePos, eyePos + lookDir, upVec);

		// update vels
		if (mMoveDir == 0)
			mMoveVel *= mMoveDec;

		if (mStrafeDir == 0) 
			mStrafeVel *= mStrafeDec;
	}

	virtual void OnMouseMove(int x, int y)
	{
		mMouseChangeX -= GetMousePositionRelative().x;
		mMouseChangeY -= GetMousePositionRelative().y;
	}

	virtual void OnKeyDown(unsigned char key)
	{
		KeyboardHandler::OnKeyDown(key);

		switch (key)
		{
			case 'w':
			{
				mMoveDir = 1.0f;
			} break;

			case 's':
			{
				mMoveDir = -1.0f;
			} break;

			case 'a':
			{
				mStrafeDir = 1.0f;
			} break;

			case 'd':
			{
				mStrafeDir = -1.0f;
			} break;

		}
	}

	virtual void OnKeyUp(unsigned char key)
	{
		KeyboardHandler::OnKeyUp(key);

		switch (key)
		{
			case 'w': 
			{
				mMoveDir = 0;
			} break;

			case 's':
			{
				mMoveDir = 0;
			} break;

			case 'a':
			{
				mStrafeDir = 0;
			} break;

			case 'd':
			{
				mStrafeDir = 0;
			} break;

		}
	}

	virtual void Disable()
	{
		InputHandler::Disable();
	}

	virtual void Enable()
	{
		InputHandler::Enable();
	}

};

#endif
