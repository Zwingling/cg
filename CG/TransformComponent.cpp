#include "TransformComponent.h"
#include "glm/gtc/matrix_transform.hpp"

TransformComponent::TransformComponent(SceneObject* owner) : GenericComponent<SceneObject>("TransformComponent", owner)
{
	mScale = glm::vec3(1,1,1);
}

void TransformComponent::Rebuild()
{
	auto scale = glm::scale(glm::mat4(1.0f), mScale);
	auto rotateX = glm::rotate(glm::mat4(1.0f), mRotation.x, glm::vec3(1.0f, 0.0f, 0.0f));
	auto rotateY = glm::rotate(glm::mat4(1.0f), mRotation.y, glm::vec3(0.0f, 1.0f, 0.0f));
	auto rotateZ = glm::rotate(glm::mat4(1.0f), mRotation.z, glm::vec3(0.0f, 0.0f, 1.0f));
	auto translate = glm::translate(glm::mat4(1.0f), mTranslation);

	mTransform = translate * rotateX * rotateY * rotateZ * scale;
}

glm::mat4 TransformComponent::GetTransform()
{
	if (mChanged)
	{
		mChanged = false;
		Rebuild();
	}

	return mTransform;
}

glm::vec3 TransformComponent::GetScale()
{
	return mScale;
}

glm::vec3 TransformComponent::GetTranslation()
{
	return mTranslation;
}

glm::vec3 TransformComponent::GetRotation()
{
	return mRotation;
}

void TransformComponent::Scale(float x, float y, float z)
{
	mScale.x += x;
	mScale.y += y;
	mScale.z += z;
	mChanged = true;
}

void TransformComponent::Scale(glm::vec3 v)
{
	mScale += v;
	mChanged = true;
}

void TransformComponent::Translate(float x, float y, float z)
{
	mTranslation.x += x;
	mTranslation.y += y;
	mTranslation.z += z;
	mChanged = true;
}

void TransformComponent::Translate(glm::vec3 v)
{
	mTranslation += v;
	mChanged = true;
}

void TransformComponent::Rotate(float x, float y, float z)
{
	mRotation.x += x;
	mRotation.y += y;
	mRotation.z += z;
	mChanged = true;
}

void TransformComponent::Rotate(glm::vec3 v)
{
	mRotation += v;
	mChanged = true;
}

void TransformComponent::SetRotate(glm::vec3 v)
{
	mRotation = v;
	mChanged = true;
}

void TransformComponent::SetTranslate(glm::vec3 v)
{
	mTranslation = v;
	mChanged = true;
}

void TransformComponent::SetScale(glm::vec3 v)
{
	mScale = v;
	mChanged = true;
}
