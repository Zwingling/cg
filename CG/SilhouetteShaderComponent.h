#ifndef SilhouetteShaderComponent_h__
#define SilhouetteShaderComponent_h__

#include "ShaderComponent.h"

class SilhouetteShaderComponent : public ShaderComponent
{
private:
	ShaderVariable mColorSV;
	ShaderVariable mMVPSV;

public:
	SilhouetteShaderComponent(FullSceneObject* owner);

	virtual void OnInit();
	virtual void Bind();
	virtual void Unbind();
};


#endif // SilhouetteShaderComponent_h__
