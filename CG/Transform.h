#ifndef Transform_H
#define Transform_H

#include "glm/glm.hpp"

class Transform
{
public:
	Transform();

	void Rotate(float x, float y, float z);
	void Translate(float x, float y, float z);
	void Scale(float x, float y, float z);

	void Rotate(glm::vec3 v);
	void Translate(glm::vec3 v);
	void Scale(glm::vec3 v);

	void SetRotation(glm::vec3 v);
	void SetTranslation(glm::vec3 v);
	void SetScale(glm::vec3 v);

	glm::vec3 GetRotation() { return mRotation; }
	glm::vec3 GetTranslation() { return mTranslation; }
	glm::vec3 GetScale() { return mScale; }

	glm::mat4 GetTransform() { return mTransform; }

private:
	void Update();

	glm::mat4 mTransform;
	glm::vec3 mRotation;
	glm::vec3 mTranslation;
	glm::vec3 mScale;
};

#endif