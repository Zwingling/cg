#include "Application.h"
#include "Scene.h"
#include "KeyboardHandler.h"
#include "ApplicationKeyboardHandler.h"
#include "MouseHandler.h"
#include "Smoother.h"

#include <GL/glew.h>
#include <GL/freeglut.h>

Application::Application()
{
    mScene = new Scene();
	mbPaused = false;
	mStop = false;
	mWarped = false;
}

Application::~Application() {
    delete mScene;
}

void Application::Initialize(unsigned int windowWidth, unsigned int windowHeight, std::string windowTitle)
{
	mScene->AddObject(new ApplicationKeyboardHandler(*this));
	
    mWindowWidth  = windowWidth;
    mWindowHeight = windowHeight;
    mWindowTitle  = windowTitle;

	mFirstInput = true;
	mUseFixRefreshRate = true;
	mFPS = 60;
	mRefreshTime = 1.0 / mFPS;
    
    int argc = 0;
    glutInit(&argc, NULL);
    glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
      
    glutInitWindowPosition(0, 0);
    glutInitWindowSize(mWindowWidth, mWindowHeight);
    glutCreateWindow(mWindowTitle.c_str());

	glutSetCursor(GLUT_CURSOR_NONE);
	glutWarpPointer(mWindowWidth / 2, mWindowWidth / 2);
	mMousePosition = glm::vec2(mWindowWidth / 2, mWindowHeight / 2);

	glutKeyboardUpFunc(CBOnKeyboardUp);

    glutKeyboardFunc(CBOnKeyboard);
    glutMouseFunc(CBOnMouse);
	glutPassiveMotionFunc(CBOnMouseMove);

    glutVisibilityFunc(CBOnVisibility);
    glutReshapeFunc(CBOnReshape);
	
	// Dark blue background to get a better contrast at the planes
	glClearColor(0.0f, 0.0f, 0.4f, 1.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);
	glewInit();

	//create texture A
	glEnable(GL_TEXTURE_2D);
	glGenTextures(1, &mRenderTarget);
	glBindTexture(GL_TEXTURE_2D, mRenderTarget);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, mWindowWidth, mWindowHeight, 0, GL_RGBA,
				 GL_UNSIGNED_BYTE, NULL);

	glGenFramebuffers(1, &mRenderTargetFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, mRenderTargetFBO);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0,
						   GL_TEXTURE_2D, mRenderTarget, 0);

	GLuint depthrenderbuffer;
	glGenRenderbuffers(1, &depthrenderbuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, depthrenderbuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, mWindowWidth, mWindowHeight);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, depthrenderbuffer);
}

void Application::Start() 
{
	bool updated = false;
	bool rendered = false;
	mElapsed = (float)mRefreshTime;
	float remainder = 0;
	Smoother<float> smoother(5, (float)mRefreshTime);

 	mTimer.Start();
	while (!mStop)
	{
// 		if (updated)
// 		{
// 			OnRender();
// 			updated = false;
// 			rendered = true;
// 		}
// 
// 		if (elapsed >= mRefreshTime)
// 		{
// 			while (elapsed >= mRefreshTime)
// 			{
// 				OnUpdate();
// 				elapsed -= (float)mRefreshTime;
// 			}
// 
// 			updated = true;			
// 		}
// 		else
// 		{
// 			glutMainLoopEvent();
// 		}
// 
// 		if (rendered)
// 		{
// 			glutSwapBuffers();
// 			rendered = false;
// 		}
// 
// 		if (mTimer.GetElapsed() > 0)
// 		{
// 			smoother.Add(mTimer.GetElapsed());
// 			mElapsed = smoother.Get();
// 			mTimer.Start();
// 		}	
	
		if (mTimer.GetElapsed() > mRefreshTime)
		{
			mElapsed = mTimer.GetElapsed();

			glutMainLoopEvent();
			OnUpdate();
			OnRender();

			glutSwapBuffers();

			mTimer.Start();
		}
	}


}

void Application::OnRender() 
{
	glClearColor(0.0f, 0.0f, 0.4f, 1.0f);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

 	glBindFramebuffer(GL_FRAMEBUFFER, mRenderTargetFBO);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    mScene->OnRender();

	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	mScene->OnPostRender();
}

void Application::OnUpdate()
{
    mScene->OnUpdate();
}

void Application::OnMouse(int button, int state, int x, int y) 
{
	if (state == GLUT_UP)
	{
		if (button == GLUT_LEFT_BUTTON)
		{
			for (auto it = mMouseHandler.begin(); it != mMouseHandler.end(); it++)
			{
				if ((*it)->IsEnabled())
					(*it)->OnLBUp();
			}
		}
		else if (button == GLUT_RIGHT_BUTTON)
		{
			for (auto it = mMouseHandler.begin(); it != mMouseHandler.end(); it++)
			{
				if ((*it)->IsEnabled())
					(*it)->OnRBUp();
			}
		}
		else if (button == GLUT_MIDDLE_BUTTON)
		{
			for (auto it = mMouseHandler.begin(); it != mMouseHandler.end(); it++)
			{
				if ((*it)->IsEnabled())
					(*it)->OnMBUp();
			}
			
		}
	}
	else
	{
		if (button == GLUT_LEFT_BUTTON)
		{
			for (auto it = mMouseHandler.begin(); it != mMouseHandler.end(); it++)
			{
				if ((*it)->IsEnabled())
					(*it)->OnLBDown();
			}
		}
		else if (button == GLUT_RIGHT_BUTTON)
		{
			for (auto it = mMouseHandler.begin(); it != mMouseHandler.end(); it++)
			{
				if ((*it)->IsEnabled())
					(*it)->OnRBDown();
			}
	
		}
		else if (button == GLUT_MIDDLE_BUTTON)
		{
			for (auto it = mMouseHandler.begin(); it != mMouseHandler.end(); it++)
			{
				if ((*it)->IsEnabled())
					(*it)->OnMBDown();
			}
		}
	}
}

void Application::OnMouseMove(int x, int y)
{
	if(!mWarped)
	{
		mMousePositionRelative = glm::vec2(x, y) - mMousePosition;
		
		if (fabs((float)mWindowWidth / 2 - x) > mWindowWidth / 4 || fabs((float)mWindowHeight / 2 - y) > mWindowHeight / 4)
		{
			glutWarpPointer(mWindowWidth / 2, mWindowHeight / 2);	
			mWarped = true;
		}
		else
		{
			mMousePosition = glm::vec2(x, y); 
		}
		
		for (auto it = mMouseHandler.begin(); it != mMouseHandler.end(); it++)
		{
			if ((*it)->IsEnabled())
				(*it)->OnMouseMove(x, y);
		}
		
	}
	else {
		mWarped = false;
		mMousePosition = glm::vec2(mWindowWidth / 2, mWindowHeight / 2);
	}
	

	//printf("%f:%f\r", mMousePositionRelative.x, mMousePositionRelative.y);


}

void Application::OnKeyboard(unsigned char key, int x, int y) 
{
	for (auto it = mKeyboardHandler.begin(); it != mKeyboardHandler.end(); it++)
	{
		if ((*it)->IsEnabled())
			(*it)->OnKeyDown(key);
	}
}

void Application::OnKeyboardUp(unsigned char key, int x, int y)
{
	for (auto it = mKeyboardHandler.begin(); it != mKeyboardHandler.end(); it++)
	{
		if ((*it)->IsEnabled())
			(*it)->OnKeyUp(key);
	}
}

void Application::OnReshape(int width, int height)
{

}

void Application::OnVisibility() 
{
	for (auto it = mKeyboardHandler.begin(); it != mKeyboardHandler.end(); it++)
		(*it)->Reset();

	mFirstInput = true;

	mMousePosition = glm::vec2(mWindowWidth / 2, mWindowHeight / 2);
	glutWarpPointer(mWindowWidth / 2, mWindowHeight / 2);
}

void Application::AddHandler(KeyboardHandler* handler)
{
	mKeyboardHandler.push_back(handler);
}

void Application::AddHandler(MouseHandler* handler)
{
	mMouseHandler.push_back(handler);
}

void Application::Stop()
{
	mStop = true;
}
