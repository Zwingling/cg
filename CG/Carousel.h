
#ifndef Carousel_h__
#define Carousel_h__

#include "ToonShadedObject.h"
#include "Geometry.h"

#include <vector>
#include <string>

class Carousel : public ToonShadedObject
{
public:
	Carousel();
	~Carousel();

private:
	std::vector<Geometry> LoadGeometry(const char* dir, const char* filename);
	FullSceneObject* CreateSceneObject(FullSceneObject* parent, std::vector<Geometry> meshes, const char* diffuseTex);
	FullSceneObject* CreateSceneObject(std::vector<Geometry> meshes, const char* diffuseTex);

	virtual void OnAttach();
};

#endif // Carousel_h__
