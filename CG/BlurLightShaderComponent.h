#ifndef BlurLightShaderComponent_h__
#define BlurLightShaderComponent_h__

#include "ShaderComponent.h"

class BlurLightShaderComponent : public ShaderComponent
{
private:
	ShaderVariable mTextureSamplerSV;
	ShaderVariable mShift;
	
public:
	BlurLightShaderComponent(FullSceneObject* owner);

	virtual void OnInit();
	virtual void Bind();
};


#endif // LightShaderComponent_h__