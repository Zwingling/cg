#ifndef CarouselBaseAnimation_h__
#define CarouselBaseAnimation_h__

#include "Component.h"
#include "glm/glm.hpp"

class CarouselBaseAnimation : public GenericComponent <class FullSceneObject>
{
public:
	CarouselBaseAnimation(FullSceneObject* owner);
	virtual void OnUpdate();

	void SetSpeed(float speed)
	{
		mSpeed = speed;
	}

private:
	float mTime;
	float mSpeed;
};

#endif // CarouselBaseAnimation_h__
