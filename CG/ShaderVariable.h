#ifndef ShaderVariable_H
#define ShaderVariable_H

#include "glm/glm.hpp"
#include <GL/glew.h>
#include <GL/freeglut.h>

class ShaderVariable
{
private:
	GLuint mId;

public:
	ShaderVariable(){}
	ShaderVariable(GLuint id)
	{
		mId = id;
	}

	void SetMatrix(float* mat)
	{
		glUniformMatrix4fv(mId, 1, GL_FALSE, mat);
	}
	void SetMatrix3(float* mat)
	{
		glUniformMatrix3fv(mId, 1, GL_FALSE, mat);
	}
	void SetVec3(glm::vec3 v)
	{
		glUniform3f(mId, v.x, v.y, v.z);
	}
	void SetVec2(glm::vec2 v)
	{
		glUniform2f(mId, v.x, v.y);
	}
	void SetSampler(unsigned int index)
	{
		glUniform1i(mId, index);
	}
	
	void SetInteger(int index)
	{
		glUniform1i(mId, index);
	}

	void SetFloat(float index)
	{
		glUniform1f(mId, index);
	}

};


#endif