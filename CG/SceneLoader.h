#ifndef SceneLoader_H_Included
#define SceneLoader_H_Included

#include <vector>
#include <string>
#include "glm/glm.hpp"

struct Mesh
{
	std::string name;

	std::vector<glm::vec3> vertices;
	std::vector<glm::vec3> normals;
	std::vector<glm::vec2> texCoords;
	std::vector<unsigned int> indices;
};

class SceneLoader
{
	SceneLoader() {}
	~SceneLoader() {}
	SceneLoader(SceneLoader const&); // Don't Implement
	void operator=(SceneLoader const&); // Don't implement

public:
	static SceneLoader& getInstance() 
	{
		static SceneLoader instance;
		return instance;
	}

	std::vector<Mesh> Load(const char* dir, const char* filename);
};


#endif