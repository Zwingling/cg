#include "SceneObject.h"
#include "Application.h"
#include "Scene.h"
#include "Component.h"

SceneObject::SceneObject()
{
	mParent = NULL;
	mId = Application::getInstance().GetScene()->GetNewId();

	mLocalTransform = AddComponent<TransformComponent>(this);
	mLocalAxis = AddComponent<TransformComponent>(this);
}

SceneObject::SceneObject(SceneObject* parent) 
{
    mParent = parent;
    mId = Application::getInstance().GetScene()->GetNewId();

	mLocalTransform = AddComponent<TransformComponent>(this);
	mLocalAxis = AddComponent<TransformComponent>(this);
}

void SceneObject::AddChild(SceneObject* object) 
{
    mObjects[object->GetId()] = object;
	object->mParent = this;;
}

void SceneObject::CBOnAttach(Scene* scene)
{
    mScene = scene;
    OnAttach();

	auto itr = mObjects.begin();
    for(; itr != mObjects.end(); itr++)
        itr->second->CBOnAttach(scene);

	auto itrC = mComponents.begin();
	for (; itrC != mComponents.end(); itrC++)
		itrC->second->OnInit();
}

void SceneObject::CBOnRender()
{
    OnRender();

	auto itr = mObjects.begin();
    for(; itr != mObjects.end(); itr++)
        itr->second->CBOnRender();
}

void SceneObject::CBOnUpdate()
{
	OnUpdate();

	auto itrC = mComponents.begin();
	for (; itrC != mComponents.end(); itrC++)
		itrC->second->OnUpdate();

	mTransformGlobal = mLocalAxis->GetTransform() * mLocalTransform->GetTransform();
	if (mParent)
		mTransformGlobal = mParent->GetGlobalTransform() * mTransformGlobal;

    auto itr = mObjects.begin();
    for(; itr != mObjects.end(); itr++)
        itr->second->CBOnUpdate();
}

Component* SceneObject::AddComponent(Component* component)
{
	mComponents[component->GetName()] = component;
	return component;
}

void SceneObject::CBOnPostRender()
{
	OnPostRender();

	auto itr = mObjects.begin();
	for (; itr != mObjects.end(); itr++)
		itr->second->OnPostRender();
}
