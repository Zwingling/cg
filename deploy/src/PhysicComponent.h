#ifndef PhysicComponent_H
#define PhysicComponent_H

#include "glm/glm.hpp"
#include "Component.h"

class FullSceneObject;
class PhysicComponent : public GenericComponent<class FullSceneObject>
{
private:
	glm::vec3 mAngluarVelocity;
	glm::vec3 mLinearVelocity;

public:
	PhysicComponent(FullSceneObject* owner);

	virtual void OnUpdate();

	glm::vec3& AngularVelocity();
	glm::vec3& LinearVelocity();
};

#endif