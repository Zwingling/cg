#ifndef BillboardShaderComponent_h__
#define BillboardShaderComponent_h__

#include "ShaderComponent.h"

class BillboardShaderComponent : public ShaderComponent
{
private:
	ShaderVariable mTextureSamplerDiffuseSV;
	ShaderVariable mTextureSamplerSpecularSV;
	ShaderVariable mTextureSamplerNormalSV;
	ShaderVariable mColorSV;

	ShaderVariable mAmbientSV;
	ShaderVariable mSpecularSV;
	ShaderVariable mDiffuseSV;

	ShaderVariable mMVPSV;
	ShaderVariable mViewSV;
	ShaderVariable mModelSV;
	ShaderVariable mModel3x3SV;
	ShaderVariable mTextureSamplerSV;

	struct LightShaderVariable
	{
		ShaderVariable position;
		ShaderVariable color;
	};

	enum { MaxNumberOfLights = 8 };
	LightShaderVariable mLights[MaxNumberOfLights];

	ShaderVariable mNumberOfLights;
	ShaderVariable mLightPosition;

	float randomThreshold;

public:
	BillboardShaderComponent(FullSceneObject* owner);

	virtual void OnInit();
	virtual void Bind();
	virtual void Unbind();
};


#endif // BillboardShaderComponent_h__