#ifndef LightObject_h__
#define LightObject_h__

#include "ShaderVariable.h"
#include "Geometry.h"
#include "FullSceneObject.h"
#include "Camera.h"
#include "Application.h"
#include "Scene.h"
#include "KeyboardHandler.h"
#include "LightShaderComponent.h"

class LightObject : public FullSceneObject, KeyboardHandler
{
private:
	ShaderComponent* mLightShaderComponent;
	ShaderComponent* mBlurShaderComponent;

	glm::vec3 mOldDiffuse;
	glm::vec3 mOldAmbient;
	glm::vec3 mOldSpecular;

	bool diffuseToggle;
	bool ambientToggle;
	bool specularToggle;

	Geometry mGeometry;
	Geometry mRectangle;
	std::string mDiffuseFilename;

public:
	LightObject();

	void SetGeometry(Geometry geom)
	{
		mGeometry = geom;
		mGeometry.Generate();
	}

	void SetDiffuse(std::string diffuse)
	{
		mDiffuseFilename = diffuse;
	}

private:
	virtual void OnAttach();

	virtual void OnRender();

	virtual void OnPostRender();

	virtual void OnUpdate() {}

	virtual void OnKeyUp(unsigned char key);
};

#endif // SimpleObject_h__