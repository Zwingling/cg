#include "MaterialComponent.h"
#include "SceneObject.h"
#include "FullSceneObject.h"
#include "ShaderComponent.h"

MaterialComponent::MaterialComponent(FullSceneObject* owner) : GenericComponent<FullSceneObject>("MaterialComponent", owner)
{
	mColor = glm::vec3(-1, -1, -1);
	mDiffuse = glm::vec3(0.9, 0.9, 0.9);
	mAmbient = glm::vec3(0.2,0.2,0.2);
	mSpecular = glm::vec3(0.2, 0.2, 0.2);
}

void MaterialComponent::SetNormalTexture(const char* filename)
{
	mNormalTextureFilename = filename; 

	std::string ext = mNormalTextureFilename.substr(mNormalTextureFilename.find_last_of('.'));
	if (ext == ".bmp" || ext == ".BMP")
		mNormalTexture = loadBMP_custom(filename);
	else
		mNormalTexture = loadDDS(filename);
}

void MaterialComponent::SetSpecularTexture(const char* filename)
{
	mSpecularTextureFilename = filename;

	std::string ext = mSpecularTextureFilename.substr(mSpecularTextureFilename.find_last_of('.'));
	if (ext == ".bmp" || ext == ".BMP")
		mSpecularTexture = loadBMP_custom(filename);
	else
		mSpecularTexture = loadDDS(filename);
}

void MaterialComponent::SetDiffuseTexture(const char* filename)
{
	mDiffuseTextureFilename = filename;
	    
	std::string ext = mDiffuseTextureFilename.substr(mDiffuseTextureFilename.find_last_of('.'));
	if (ext == ".bmp" || ext == ".BMP")
		mDiffuseTexture = loadBMP_custom(filename);
	else
		mDiffuseTexture = loadDDS(filename);
} 
 
void MaterialComponent::OnInit()
{   
 	    
}   
  
void MaterialComponent::SetColor(glm::vec3 color)
{
	mColor = color;
} 

void MaterialComponent::SetAmbient(glm::vec3 color)
{ 
	mAmbient = color;
}

void MaterialComponent::SetSpecular(glm::vec3 color)
{
	mSpecular = color;
}

void MaterialComponent::SetDiffuse(glm::vec3 color)
{
	mDiffuse = color;
} 
