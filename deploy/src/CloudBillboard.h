#ifndef CLOUDBILLBOARD_H
#define CLOUDBILLBOARD_H

#include "ToonShadedObject.h"
#include "Geometry.h"

#include <vector>
#include <string>

class CloudBillboard : public ToonShadedObject
{
public:
	CloudBillboard(float transX, float transY, float transZ);
	~CloudBillboard();

private:
	float xTranslate;
	float yTranslate;
	float zTranslate;
	std::vector<Geometry> LoadGeometry(const char* dir, const char* filename);
	FullSceneObject* CreateSceneObject(FullSceneObject* parent, std::vector<Geometry> meshes, const char* diffuseTex);
	FullSceneObject* CreateSceneObject(std::vector<Geometry> meshes, const char* diffuseTex);

	virtual void OnAttach();
};

#endif // STATICSCENEOBJECT_H
