/*
* File:   SceneObject.h
* Author: mathias
*
* Created on March 22, 2015, 5:43 PM
*/

#ifndef FULLSCENEOBJECT_H
#define	FULLSCENEOBJECT_H

#include <cstdlib>
#include <string>
#include <map>

#include "PhysicComponent.h"
#include "MaterialComponent.h"
#include "SceneObject.h"

class Scene;
class FullSceneObject : public SceneObject
{
	friend class Scene;

protected:
	PhysicComponent* mPhysicComponent;
	MaterialComponent* mMaterialComponent;

public:
	FullSceneObject();
	FullSceneObject(SceneObject* parent);

	virtual void OnAttach() {}
	virtual void OnRender() {}
	virtual void OnUpdate() {}

	SceneObject* GetParent() { return mParent; }
	unsigned int GetId() { return mId; }

	PhysicComponent*    GetPhysicComponent()		  { return mPhysicComponent; }
	MaterialComponent*	GetMaterialComponent()		  { return mMaterialComponent; }

private:
	void CBOnAttach(Scene* scene);
	void CBOnRender();
	void CBOnUpdate();
};

#endif	/* FULLSCENEOBJECT_H */

