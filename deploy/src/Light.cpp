#include "Light.h"
#include "Geometry.h"
#include "SceneLoader.h"
#include "Camera.h"
#include "Application.h"
#include "Scene.h"
#include "CarouselFishAnimation.h"
#include "CarouselBaseAnimation.h"
#include "LightObject.h"


#include "glm/gtc/constants.hpp"


Light::Light(glm::vec3 color)
{
	mColor = color;
}


Light::~Light()
{

}

FullSceneObject* Light::CreateSceneObject(std::vector<Geometry> geometry, const char* diffuseTex)
{
	return CreateSceneObject(this, geometry, diffuseTex);
}

FullSceneObject* Light::CreateSceneObject(Light* parent, std::vector<Geometry> geometry, const char* diffuseTex)
{
	for (auto it = geometry.begin(); it != geometry.end(); it++)
	{
		auto pChild = new LightObject();
		pChild->SetGeometry(*it);
		pChild->SetDiffuse(std::string("resource/models/") + diffuseTex);
		parent->AddChild(pChild);
	}

	return parent;
}

void Light::OnAttach()
{
	auto scale = 0.6f;

	auto baseGeometry = LoadGeometry("resource/models/", "sphere.obj");

	CreateSceneObject(this, baseGeometry, "sphere.bmp");
	this->GetLocalTransformComponent()->SetScale(glm::vec3(scale, scale, scale));
	this->GetMaterialComponent()->SetDiffuse(glm::vec3(1,0,0));
}

std::vector<Geometry> Light::LoadGeometry(const char* dir, const char* filename)
{
	auto meshes = SceneLoader::getInstance().Load(dir, filename);
	std::vector<Geometry> geometry;

	for (auto it = meshes.begin(); it != meshes.end(); it++)
	{
		Geometry geom;

		geom.AddIndices((*it).indices.data(), (*it).indices.size());

		for (unsigned int i = 0; i < (*it).vertices.size(); i++)
		{
			geom.AddVertex((*it).vertices[i]);

			if (!(*it).normals.empty())
				geom.AddNormal((*it).normals[i]);

			if (!(*it).texCoords.empty())
				geom.AddTexturCoord((*it).texCoords[i]);
		}

		geometry.push_back(geom);
	}

	return geometry;
}
