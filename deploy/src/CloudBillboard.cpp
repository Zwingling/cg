#include "CloudBillboard.h"
#include "Geometry.h"
#include "SceneLoader.h"
#include "Camera.h"
#include "Application.h"
#include "Scene.h"
#include "BillboardObject.h"
#include "glm/gtc/constants.hpp"


CloudBillboard::CloudBillboard(float transX, float transY, float transZ)
{
	xTranslate = transX;
	yTranslate = transY;
	zTranslate = transZ;
}


CloudBillboard::~CloudBillboard()
{}

FullSceneObject* CloudBillboard::CreateSceneObject(std::vector<Geometry> geometry, const char* diffuseTex)
{
	return CreateSceneObject(new FullSceneObject(), geometry, diffuseTex);
}

FullSceneObject* CloudBillboard::CreateSceneObject(FullSceneObject* parent, std::vector<Geometry> geometry, const char* diffuseTex)
{
	for (auto it = geometry.begin(); it != geometry.end(); it++)
	{
		auto pChild = new BillboardObject();
		pChild->SetGeometry(*it);
		parent->AddChild(pChild);
	}

	return parent;
}

void CloudBillboard::OnAttach()
{
	const float baseScale = 40.0f;
	auto baseGeometry = LoadGeometry("resource/models/", "billboard.obj");
	CreateSceneObject(this, baseGeometry, "");
	this->GetLocalTransformComponent()->SetScale(glm::vec3(baseScale, baseScale, baseScale*2));
	this->GetLocalTransformComponent()->SetTranslate(glm::vec3(xTranslate, yTranslate, zTranslate));
	this->GetLocalTransformComponent()->SetRotate(glm::vec3(-glm::pi<float>() / 2, glm::pi<float>() / 2, 0));
}

std::vector<Geometry> CloudBillboard::LoadGeometry(const char* dir, const char* filename)
{
	auto meshes = SceneLoader::getInstance().Load(dir, filename);
	std::vector<Geometry> geometry;
	for (auto it = meshes.begin(); it != meshes.end(); it++)
	{
		Geometry geom;

		geom.AddIndices((*it).indices.data(), (*it).indices.size());

		for (unsigned int i = 0; i < (*it).vertices.size(); i++)
		{
			geom.AddVertex((*it).vertices[i]);

			if (!(*it).normals.empty())
				geom.AddNormal((*it).normals[i]);

			if (!(*it).texCoords.empty())
				geom.AddTexturCoord((*it).texCoords[i]);
		}
		geometry.push_back(geom);
	}

	return geometry;
}
