#ifndef AnimationComponent_h__
#define AnimationComponent_h__

#include "Component.h"
#include "glm/glm.hpp"

class CarouselFishAnimation : public GenericComponent < class FullSceneObject >
{
public:	
	CarouselFishAnimation(FullSceneObject* owner);
	virtual void OnUpdate();

	CarouselFishAnimation* SetSpeed(float speed)
	{
		mSpeed = speed;
		return this;
	}
	CarouselFishAnimation* SetRange(float range)
	{
		mRange = range;
		return this;
	}
	CarouselFishAnimation* SetOffset(float offset)
	{
		mOffset = offset;
		return this;
	}

private:
	glm::vec3 mOldTranslation;
	float mTime;
	float mSpeed;
	float mRange;
	float mOffset;
};

#endif 
AnimationComponent_h__