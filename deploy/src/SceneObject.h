/* 
 * File:   SceneObject.h
 * Author: mathias
 *
 * Created on March 22, 2015, 5:43 PM
 */

#ifndef SCENEOBJECT_H
#define	SCENEOBJECT_H

#include <cstdlib>
#include <string>
#include <map>

#include "TransformComponent.h"

class Scene;
//class Component;

class SceneObject
{
    friend class Scene;
    
protected:
	TransformComponent* mLocalTransform;
	TransformComponent* mLocalAxis;

	glm::mat4 mTransformGlobal;
    Scene* mScene;
    SceneObject* mParent;
    std::map<unsigned int, SceneObject*> mObjects;
	std::map<std::string, Component*> mComponents;
    unsigned int mId;
    

public:
    SceneObject();
    SceneObject(SceneObject* parent);
    
    void AddChild(SceneObject* object);
    
    virtual void OnAttach() {}
    virtual void OnRender() {}
	virtual void OnPostRender() {}
	virtual void OnUpdate() {}
    
    SceneObject* GetParent() { return mParent; }
    unsigned int GetId() { return mId; }

	Component* AddComponent(Component* component);
	template<class T, class OwnerType>
	T* AddComponent(OwnerType* owner)
	{
		T* component = new T(owner);
		AddComponent(component);
		return component;
	}

	TransformComponent* GetLocalTransformComponent()  { return mLocalTransform; }
	TransformComponent* GetLocalAxisComponent()		  { return mLocalAxis; }
	glm::mat4			GetGlobalTransform()		  { return mTransformGlobal; }

private:
    void CBOnAttach(Scene* scene);
    void CBOnRender();
	void CBOnPostRender();
	void CBOnUpdate();
};

#endif	/* SCENEOBJECT_H */

