/* 
 * File:   Scene.h
 * Author: mathias
 *
 * Created on March 22, 2015, 5:43 PM
 */

#ifndef SCENE_H
#define	SCENE_H

#include <cstdlib>
#include <string>
#include <map>
#include <vector>


class Camera;
class SceneObject;
class Light;

class Scene
{
private:
	Camera* mActiveCamera;
	std::map<std::string, Camera*> mCameras;
	std::vector<Light*> mLights;
    std::map<unsigned int, SceneObject*> mObjects;
    int mCurrentID;
    
public:
    Scene();
    
    void OnRender();
	void OnPostRender();
	void OnUpdate();

	Camera* AddCamera(Camera* camera);
	bool SetActiveCamera(const char* name);
	Camera* GetActiveCamera() { return mActiveCamera; }
	Camera* GetCamera(std::string name) { return mCameras.at(name); }

	Light* AddLight(Light* light);
	std::vector<Light*> GetLights() { return mLights; }

    void AddObject(SceneObject* object);  
    unsigned int GetNewId();
};

#endif	/* SCENE_H */

