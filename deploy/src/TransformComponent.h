#ifndef TransformComponent_H
#define TransformComponent_H

#include "glm/glm.hpp"
#include "Component.h"

class TransformComponent : public GenericComponent<class SceneObject>
{
private:
	glm::mat4 mTransform;
	glm::vec3 mRotation;
	glm::vec3 mTranslation;
	glm::vec3 mScale;
	bool mChanged;

public:
	TransformComponent(SceneObject* owner);

	void SetRotate(glm::vec3 v);
	void SetTranslate(glm::vec3 v);
	void SetScale(glm::vec3 v);

	void Rotate(glm::vec3 v);
	void Translate(glm::vec3 v);
	void Scale(glm::vec3 v);

	void Rotate(float x, float y, float z);
	void Translate(float x, float y, float z);
	void Scale(float x, float y, float z);

	glm::vec3 GetRotation();
	glm::vec3 GetTranslation();
	glm::vec3 GetScale();
	glm::mat4 GetTransform();

	void Rebuild();
};

#endif