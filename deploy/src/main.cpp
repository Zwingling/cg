#include "Application.h"
#include "Scene.h"
#include "Geometry.h"
#include "FreeCamera.h"
#include "RotationCamera.h"
#include "FullSceneObject.h"
#include "SceneLoader.h"
#include "Carousel.h"
#include "Light.h"
#include "StaticSceneObject.h"
#include "LightVAnimation.h"
#include "GuidedCamera.h"
#include "CloudBillboard.h"
#include "glm/gtc/constants.hpp"

int main(int argc, char** argv)
{
	Application::getInstance().Initialize(800, 600, "Merry Go Round");

	auto pScene = Application::getInstance().GetScene();

	RotationCamera* cameraRotation = new RotationCamera("rotation");
	cameraRotation->SetPerspective(45.0f, Application::getInstance().GetAspectRatio(), 0.1f, 1000.0f);
	cameraRotation->SetPosition(glm::vec3(0, 5, -30));
	
	FreeCamera* cameraFree = new FreeCamera("free");
	cameraFree->SetPerspective(45.0f, Application::getInstance().GetAspectRatio(), 0.1f, 1000.0f);
	cameraFree->SetEyePos(glm::vec3(0, 5, -30));

	GuidedCamera* cameraGuided = new GuidedCamera("guided");
	cameraGuided->SetPerspective(45.0f, Application::getInstance().GetAspectRatio(), 0.1f, 1000.0f);
	cameraGuided->GetPathAnimation()
		->AddPoint(0.00, glm::vec3(-0.22, 9.04, -48.68), glm::vec3(0.00, 0.00, 0.00), glm::vec3(-0.20, 8.85, -47.70), glm::vec3(0.00, 0.00, 0.00))
		->AddPoint(2.00, glm::vec3(15.77, 3.19, -23.58), glm::vec3(15.99, -5.84, 25.10), glm::vec3(15.15, 3.10, -22.80), glm::vec3(15.35, -5.75, 24.90))
		->AddPoint(4.00, glm::vec3(22.70, 3.13, -14.26), glm::vec3(6.93, -0.06, 9.32), glm::vec3(21.87, 3.12, -13.70), glm::vec3(6.72, 0.02, 9.10))
		->AddPoint(6.00, glm::vec3(17.17, 10.05, 19.75), glm::vec3(-5.53, 6.91, 34.01), glm::vec3(16.57, 9.80, 18.99), glm::vec3(-5.30, 6.68, 32.69))
		->AddPoint(8.00, glm::vec3(-11.88, 2.78, 7.05), glm::vec3(-29.05, -7.26, -12.70), glm::vec3(-11.03, 2.96, 6.54), glm::vec3(-27.60, -6.84, -12.45))
		->AddPoint(10.00, glm::vec3(-10.10, 2.20, -8.80), glm::vec3(1.78, -0.58, -15.85), glm::vec3(-9.35, 2.48, -8.19), glm::vec3(1.68, -0.48, -14.74))
		->AddPoint(12.00, glm::vec3(-11.50, 7.37, -4.54), glm::vec3(-1.40, 5.17, 4.26), glm::vec3(-11.59, 6.84, -5.38), glm::vec3(-2.24, 4.36, 2.82))
		->AddPoint(14.00, glm::vec3(-18.83, 3.36, -10.16), glm::vec3(-7.33, -4.01, -5.63), glm::vec3(-18.05, 3.06, -10.71), glm::vec3(-6.46, -3.78, -5.34))
		->AddPoint(16.00, glm::vec3(-16.88, 3.36, -20.74), glm::vec3(1.95, 0.00, -10.58), glm::vec3(-16.30, 3.18, -19.95), glm::vec3(1.75, 0.12, -9.24))
		->AddPoint(18.00, glm::vec3(0.76, 9.92, -47.71), glm::vec3(17.64, 6.56, -26.97), glm::vec3(0.75, 9.70, -46.74), glm::vec3(17.05, 6.52, -26.79))
		->AddPoint(20.00, glm::vec3(-0.22, 9.04, -48.68), glm::vec3(0.00, 0.00, 0.00), glm::vec3(-0.20, 8.85, -47.70), glm::vec3(0.00, 0.00, 0.00));

	pScene->AddCamera(cameraFree);
	pScene->AddCamera(cameraRotation);
	pScene->AddCamera(cameraGuided);

	cameraGuided->GetPathAnimation()->Start(true);

	pScene->SetActiveCamera(cameraGuided->GetName());
	
	pScene->AddObject(new Carousel());
	pScene->AddObject(new StaticSceneObject());
	// draw clouds in the correct order (highest z-value first)
	pScene->AddObject(new CloudBillboard(-100.0f, 130.0f, 150.0f));
	pScene->AddObject(new CloudBillboard(100.0f, 125.0f, 148.0f));
	pScene->AddObject(new CloudBillboard(0.0f, 120.0f, 145.0f));
	pScene->AddObject(new CloudBillboard(30.0f, 140.0f, 142.0f));
	pScene->AddObject(new CloudBillboard(-90.0f, 150.0f, 141.0f));

	auto light = new Light(glm::vec3(0.0f, 0.2f, 0.0f));
	light->GetLocalTransformComponent()->SetTranslate(glm::vec3(-20, 15, -20));
	float lightSpeed = 1.5f;
	float lightRange = 4.0f;
	light->AddComponent<LightVAnimation>(light)
		->SetRange(lightRange)
		->SetSpeed(lightSpeed)
		->SetOffset(0);

	pScene->AddLight(light);
	pScene->AddObject(light);

	light = new Light(glm::vec3(0.0f, 0.2f, 0.2f));
	light->GetLocalTransformComponent()->SetTranslate(glm::vec3(20, 15, -20));
	lightSpeed = 0.8f;
	lightRange = 8.0f;
	light->AddComponent<LightVAnimation>(light)
		->SetRange(lightRange)
		->SetSpeed(lightSpeed)
		->SetOffset(0);

	pScene->AddLight(light);
	pScene->AddObject(light);
	
	Application::getInstance().Start();

	return 0;
}
