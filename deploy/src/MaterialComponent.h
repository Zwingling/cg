#ifndef MaterialComponent_H
#define MaterialComponent_H

#include "Component.h"
#include "ShaderVariable.h"
#include "texture.hpp"

class MaterialComponent : public GenericComponent<class FullSceneObject>
{
private:
	std::string mDiffuseTextureFilename;
	std::string mSpecularTextureFilename;
	std::string mNormalTextureFilename;

	GLuint mDiffuseTexture;
	GLuint mSpecularTexture;
	GLuint mNormalTexture;
	
	float	  mTransparency;
	glm::vec3 mAmbient;
	glm::vec3 mSpecular;
	glm::vec3 mDiffuse;
	glm::vec3 mEmessive;
	glm::vec3 mColor;

public:
	MaterialComponent(FullSceneObject* owner);

	virtual void OnInit();

	void SetDiffuseTexture(const char* filename);
	void SetSpecularTexture(const char* filename);
	void SetNormalTexture(const char* filename);
	void SetColor(glm::vec3 color);
	void SetDiffuse(glm::vec3 color);
	void SetAmbient(glm::vec3 color);
	void SetSpecular(glm::vec3 color);

	glm::vec3 GetColor() { return mColor; }
	glm::vec3 GetDiffuse() { return mDiffuse; }
	glm::vec3 GetAmbient() { return mAmbient; }
	glm::vec3 GetSpecular() { return mSpecular; }
	glm::vec3 GetEmissive() { return mEmessive; }

	GLuint GetDiffuseTexture() { return mDiffuseTexture; }
	GLuint GetSpecularTexture() { return mSpecularTexture; }
	GLuint GetNormalTexture() { return mNormalTexture; }
};


#endif