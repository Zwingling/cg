#include "LightVAnimation.h"
#include "FullSceneObject.h"
#include "Application.h"
#include "Light.h"

LightVAnimation::LightVAnimation(FullSceneObject* owner) : GenericComponent<FullSceneObject>("LightVAnimation", owner)
{
	mOldTranslation = owner->GetLocalTransformComponent()->GetTranslation();
}

void LightVAnimation::OnUpdate()
{
	auto transform = mOwner->GetLocalTransformComponent();

	mTime += (float)Application::getInstance().GetElapsed();

	auto v = mRange * glm::sin(mTime * mSpeed + mOffset);

	transform->SetTranslate(glm::vec3(mOldTranslation.x, mOldTranslation.y + v, mOldTranslation.z));
}
