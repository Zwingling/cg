#include "FullSceneObject.h"
#include "Application.h"
#include "Scene.h"
#include "Component.h"

FullSceneObject::FullSceneObject() : SceneObject(NULL)
{
	mPhysicComponent = AddComponent<PhysicComponent>(this);
	mMaterialComponent = AddComponent<MaterialComponent>(this);
}

FullSceneObject::FullSceneObject(SceneObject* parent)
{
	mParent = parent;
	mId = Application::getInstance().GetScene()->GetNewId();
}

