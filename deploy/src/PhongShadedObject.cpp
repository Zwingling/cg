#include "PhongShadedObject.h"
#include "Light.h"
#include <vector>

PhongShadedObject::PhongShadedObject()
{
	mShaderComponent = AddComponent<PhongShaderComponent>(this);
}

void PhongShadedObject::OnRender()
{
	mShaderComponent->Bind();
	mGeometry.Render();
}

void PhongShadedObject::OnAttach()
{
	if (mDiffuseFilename != "")
	{
		GetMaterialComponent()->SetDiffuseTexture(mDiffuseFilename.c_str());
	}
	else
	{
		GetMaterialComponent()->SetDiffuseTexture("resource/models/uvmap.DDS");
	}

	this->ambientToggle = true;
	this->diffuseToggle = true;
	this->specularToggle = true;

	mOldAmbient = GetMaterialComponent()->GetAmbient();
	mOldDiffuse = GetMaterialComponent()->GetDiffuse();
	mOldSpecular = GetMaterialComponent()->GetSpecular();
}

void PhongShadedObject::OnKeyUp(unsigned char key)
{
	KeyboardHandler::OnKeyUp(key);

	switch (key)
	{
	case 'u':
	{
		if (diffuseToggle)
		{
			mOldDiffuse = this->GetMaterialComponent()->GetDiffuse();
			this->GetMaterialComponent()->SetDiffuse(glm::vec3(0.0, 0.0, 0.0));
		}
		else
		{
			this->GetMaterialComponent()->SetDiffuse(mOldDiffuse);
		}
		diffuseToggle = !diffuseToggle;
		break;
	}
	case 'i':
	{
		if (ambientToggle)
		{
			mOldAmbient = this->GetMaterialComponent()->GetAmbient();
			this->GetMaterialComponent()->SetAmbient(glm::vec3(0.0, 0.0, 0.0));
		}
		else
		{
			this->GetMaterialComponent()->SetAmbient(mOldAmbient);
		}
		ambientToggle = !ambientToggle;
		break;
	}
	case 'o':
	{
		if (specularToggle)
		{
			mOldSpecular = this->GetMaterialComponent()->GetSpecular();
			this->GetMaterialComponent()->SetSpecular(glm::vec3(0.0, 0.0, 0.0));
		}
		else
		{
			this->GetMaterialComponent()->SetSpecular(mOldSpecular);
		}
		specularToggle = !specularToggle;
		break;
	}
	}
}
