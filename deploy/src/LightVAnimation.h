#ifndef LightVAnimationComponent_h__
#define LightVAnimationComponent_h__

#include "Component.h"
#include "glm/glm.hpp"

class LightVAnimation : public GenericComponent < class FullSceneObject >
{
public:	
	LightVAnimation(FullSceneObject* owner);
	virtual void OnUpdate();

	LightVAnimation* SetSpeed(float speed)
	{
		mSpeed = speed;
		return this;
	}
	LightVAnimation* SetRange(float range)
	{
		mRange = range;
		return this;
	}
	LightVAnimation* SetOffset(float offset)
	{
		mOffset = offset;
		return this;
	}

private:
	glm::vec3 mOldTranslation;
	float mTime;
	float mSpeed;
	float mRange;
	float mOffset;
};

#endif 
