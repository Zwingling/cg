#ifndef Geometry_H_included
#define Geometry_H_included

#include <GL/glew.h>
#include <GL/freeglut.h>
#include "glm/glm.hpp"
#include <vector>

#include "objloader.hpp"
#include "vboindexer.hpp"
#include "tangentspace.hpp"

class Geometry
{
private:
	GLuint mVertexArrayID;
	GLuint mVertexBuffer;
	GLuint mNormalBuffer;
	GLuint mTangentBuffer;
	GLuint mBiTangentBuffer;
	GLuint mUVBuffer;
	GLuint mIndexBuffer;
	std::vector<glm::vec3> mNormals;
	std::vector<glm::vec3> mTangents;
	std::vector<glm::vec3> mBiTangents;
	std::vector<glm::vec2> mTextureCoords;
	std::vector<glm::vec3> mVertices;
	std::vector<unsigned int> mIndices;

public:
	Geometry()
	{
		glGenVertexArrays(1, &mVertexArrayID);
		glGenBuffers(1, &mVertexBuffer);
		glGenBuffers(1, &mNormalBuffer);
		glGenBuffers(1, &mTangentBuffer);
		glGenBuffers(1, &mBiTangentBuffer);
		glGenBuffers(1, &mUVBuffer);
		glGenBuffers(1, &mIndexBuffer);
	}

	void Generate()
	{
		glBindVertexArray(mVertexArrayID);

		glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*mVertices.size(), mVertices.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, mNormalBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*mNormals.size(), mNormals.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, mTangentBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*mTangents.size(), mTangents.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, mBiTangentBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec3)*mBiTangents.size(), mBiTangents.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ARRAY_BUFFER, mUVBuffer);
		glBufferData(GL_ARRAY_BUFFER, sizeof(glm::vec2)*mTextureCoords.size(), mTextureCoords.data(), GL_STATIC_DRAW);

		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, mIndices.size() * sizeof(unsigned int), mIndices.data(), GL_STATIC_DRAW);

	}

	void Render()
	{
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, mIndexBuffer);

		// 1rst attribute buffer : vertices
		glEnableVertexAttribArray(0);
		glBindBuffer(GL_ARRAY_BUFFER, mVertexBuffer);
		glVertexAttribPointer(
			0, // attribute 0. No particular reason for 0, but must match the layout in the shader.
			3, // size
			GL_FLOAT, // type
			GL_FALSE, // normalized?
			0, // stride
			(void*)0 // array buffer offset
			);


		// 2nd attribute buffer : UVs
		glEnableVertexAttribArray(1);
		glBindBuffer(GL_ARRAY_BUFFER, mUVBuffer);
		glVertexAttribPointer(
			1,                                // attribute
			2,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		// 3rd attribute buffer : normals
		glEnableVertexAttribArray(2);
		glBindBuffer(GL_ARRAY_BUFFER, mNormalBuffer);
		glVertexAttribPointer(
			2,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		// 3rd attribute buffer : normals
		glEnableVertexAttribArray(3);
		glBindBuffer(GL_ARRAY_BUFFER, mTangentBuffer);
		glVertexAttribPointer(
			3,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);


		// 3rd attribute buffer : normals
		glEnableVertexAttribArray(4);
		glBindBuffer(GL_ARRAY_BUFFER, mBiTangentBuffer);
		glVertexAttribPointer(
			4,                                // attribute
			3,                                // size
			GL_FLOAT,                         // type
			GL_FALSE,                         // normalized?
			0,                                // stride
			(void*)0                          // array buffer offset
			);

		// Draw the triangle !
		// Draw the triangles !
		glDrawElements(
			GL_TRIANGLES,      // mode
			mIndices.size(),    // count
			GL_UNSIGNED_INT,   // type
			(void*)0           // element array buffer offset
			);

		glDisableVertexAttribArray(0);
		glDisableVertexAttribArray(1);
		glDisableVertexAttribArray(2);
		glDisableVertexAttribArray(3);
		glDisableVertexAttribArray(4);
	}

	void AddNormal(glm::vec3 v)
	{
		mNormals.push_back(v);
	}
	void AddTexturCoord(glm::vec2 v)
	{
		mTextureCoords.push_back(v);
	}
	void AddTangent(glm::vec3 v)
	{
		mTangents.push_back(v);
	}
	void AddBiTangent(glm::vec3 v)
	{
		mBiTangents.push_back(v);
	}
	void AddVertex(glm::vec3 v)
	{
		mVertices.push_back(v);
	}
	void AddIndex(unsigned int i)
	{
		mIndices.push_back(i);
	}
	void AddIndices(unsigned int* indices, unsigned int count)
	{
		for (size_t i = 0; i < count; i++)
			mIndices.push_back(indices[i]);
	}
};

static Geometry CreateCube()
{
	Geometry geom;
	geom.AddVertex(glm::vec3(-1.0, -1.0, 1.0));
	geom.AddVertex(glm::vec3(1.0, -1.0, 1.0));
	geom.AddVertex(glm::vec3(1.0, 1.0, 1.0));
	geom.AddVertex(glm::vec3(-1.0, 1.0, 1.0));

	geom.AddVertex(glm::vec3(-1.0, -1.0, -1.0));
	geom.AddVertex(glm::vec3(1.0, -1.0, -1.0));
	geom.AddVertex(glm::vec3(1.0, 1.0, -1.0));
	geom.AddVertex(glm::vec3(-1.0, 1.0, -1.0));


	unsigned int cube_elements[] = {
		// front
		0, 1, 2,
		2, 3, 0,
		// top
		3, 2, 6,
		6, 7, 3,
		// back
		7, 6, 5,
		5, 4, 7,
		// bottom
		4, 5, 1,
		1, 0, 4,
		// left
		4, 0, 3,
		3, 7, 4,
		// right
		1, 5, 6,
		6, 2, 1,
	};

	geom.AddIndices(cube_elements, 36);

	return geom;
}
static Geometry CreatePlane()
{
	Geometry geom;
	geom.AddVertex(glm::vec3(-0.5f, 0.0, -0.5));
	geom.AddVertex(glm::vec3(0.5, 0.0, -0.5));
	geom.AddVertex(glm::vec3(0.5, 0.0, 0.5));
	geom.AddVertex(glm::vec3(-0.5, 0.0, 0.5));


	unsigned int cube_elements[] = {
		// front
		2, 1, 0,
		0, 3, 2,
	};

	geom.AddIndices(cube_elements, 6);

	return geom;
}



static Geometry LoadObj(const char* filename)
{
	// Read our .obj file
	std::vector<glm::vec3> vertices;
	std::vector<glm::vec2> uvs;
	std::vector<glm::vec3> normals;
	bool res = loadOBJ(filename, vertices, uvs, normals);

	std::vector<glm::vec3> tangents;
	std::vector<glm::vec3> bitangents;
	computeTangentBasis(
		vertices, uvs, normals, // input
		tangents, bitangents    // output
		);

	std::vector<unsigned short> indices;
	std::vector<glm::vec3> indexed_vertices;
	std::vector<glm::vec2> indexed_uvs;
	std::vector<glm::vec3> indexed_normals;
	std::vector<glm::vec3> indexed_tangents;
	std::vector<glm::vec3> indexed_bitangents;
	indexVBO_TBN(
		vertices, uvs, normals, tangents, bitangents,
		indices, indexed_vertices, indexed_uvs, indexed_normals, indexed_tangents, indexed_bitangents
		);

	Geometry geom;

	for (unsigned int i = 0; i < indices.size(); i++)
	{
		geom.AddIndex(indices[i]);
	}

	for (unsigned int i = 0; i < indexed_vertices.size(); i++)
	{
		geom.AddVertex(indexed_vertices[i]);
		geom.AddNormal(indexed_normals[i]);
		geom.AddTangent(indexed_tangents[i]);
		geom.AddBiTangent(indexed_bitangents[i]);
		geom.AddTexturCoord(indexed_uvs[i]);
	}


	return geom;
}

#endif