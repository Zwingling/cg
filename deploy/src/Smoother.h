#include <vector>

template<typename Rep>
class Smoother
{
public:
	Smoother(int size, float defaultVal)
	{
		mSize = size;
		mDefault = defaultVal;
	}

	void Add(Rep val)
	{
		if (mCurr >= mSize)
		{
			mCurr = 0;
		}

		mData.push_back(val);
		mCurr++;
	}

	Rep Get()
	{
		if (mData.empty())
		{
			return mDefault;
		}

		Rep sum = Rep();

		for (unsigned int i = 0; i < mData.size(); i++)
		{
			sum += mData[i];
		}

		return sum / mData.size();
	}

private:
	std::vector<Rep> mData;
	Rep mDefault;
	int mSize;
	int mCurr;
};