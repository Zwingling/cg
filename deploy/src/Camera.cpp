#include "Camera.h"
#include "glm/gtc/matrix_transform.hpp"
#include "Application.h"

Camera::Camera(const char* name)
{
	mName = name;
	mView = glm::mat4(1.0f);
	mProjection = glm::mat4(1.0f);

	mLookAt = glm::vec3(0, 0, 0);
	mEyePos = glm::vec3(0, 0, 1);
	mUp = glm::vec3(0, 1, 0);
}

void Camera::SetPerspective(float degrees, float aspect, float fNear, float fFar)
{
	mFov = degrees;
	mAspect = aspect;
	mNear = fNear;
	mFar = fFar;
	mProjection = glm::perspective(degrees, aspect, fNear, fFar);
}

void Camera::LookAt(glm::vec3 position, glm::vec3 target, glm::vec3 up)
{
	mLookAt = target;
	mEyePos = position;
	mUp = up;

	mView = glm::lookAt(position, target, up);
}
