#include "PhysicComponent.h"
#include "SceneObject.h"
#include "Application.h"
#include "FullSceneObject.h"

PhysicComponent::PhysicComponent(FullSceneObject* owner) : GenericComponent<FullSceneObject>("PhysicComponent", owner)
{

}

glm::vec3& PhysicComponent::LinearVelocity()
{
	return mLinearVelocity;
}

glm::vec3& PhysicComponent::AngularVelocity()
{
	return mAngluarVelocity;
}

void PhysicComponent::OnUpdate()
{
	auto transform = mOwner->GetLocalAxisComponent();

	auto t = (float)Application::getInstance().GetElapsed();

	auto linear = mLinearVelocity * t;
	auto angular = mAngluarVelocity * t;

	transform->Translate(linear);
	transform->Rotate(angular);
}
