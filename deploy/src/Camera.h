#ifndef Camera_H
#define Camera_H

#include "glm/glm.hpp"
#include <string>
#include <cstring>

#include "SceneObject.h"

class Camera : public SceneObject
{
private:
	std::string mName;

	glm::mat4 mView;
	glm::mat4 mProjection;
	glm::vec3 mLookAt;
	glm::vec3 mEyePos;
	glm::vec3 mUp;

	float mFov;
	float mAspect;
	float mNear;
	float mFar;

public:
	Camera(const char* name);

	const char* GetName() { return mName.c_str(); }
	glm::mat4 GetView() { return mView; }
	glm::mat4 GetProjection() { return mProjection; }

	glm::vec3 GetLookAt() { return mLookAt; }
	glm::vec3 GetEyePos() { return mEyePos; }
	glm::vec3 GetUp() { return mUp; }

	void SetEyePos(glm::vec3 pos)
	{
		auto dir = GetEyePos() - GetLookAt();
		LookAt(pos, pos + dir, GetUp());
	}
	void SetLookAt(glm::vec3 pos)
	{
		auto dir = GetEyePos() - GetLookAt();
		LookAt(pos, pos + dir, GetUp());
	}

	void SetPerspective(float degrees, float aspect, float near, float far);
	void LookAt(glm::vec3 position, glm::vec3 target, glm::vec3 up);

	float GetFov()		const { return mFov; }
	float GetAspect()	const { return mAspect; }
	float GetNear()		const { return mNear; }
	float GetFar()		const { return mFar; }

	virtual void Disable() { };
	virtual void Enable() { };
};


#endif