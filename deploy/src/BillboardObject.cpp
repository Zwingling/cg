#include "BillboardObject.h"
#include <vector>
#include "BillboardShaderComponent.h"

BillboardObject::BillboardObject()
{
	mShaderComponent = AddComponent<BillboardShaderComponent>(this);
}


void BillboardObject::OnRender()
{	
	mShaderComponent->Bind();
	mGeometry.Render();
	mShaderComponent->Unbind();
}

void BillboardObject::OnAttach()
{
	GetMaterialComponent()->SetDiffuseTexture("resource/models/cloud1.bmp");
}
