#include "StaticSceneObject.h"
#include "Geometry.h"
#include "SceneLoader.h"
#include "Camera.h"
#include "Application.h"
#include "Scene.h"
#include "PhongShadedObject.h"
#include "ToonShadedObject.h"

#include "glm/gtc/constants.hpp"


StaticSceneObject::StaticSceneObject()
{} 


StaticSceneObject::~StaticSceneObject()
{}

FullSceneObject* StaticSceneObject::CreateSceneObject(std::vector<Geometry> geometry, const char* diffuseTex)
{
	return CreateSceneObject(new FullSceneObject(), geometry, diffuseTex);
} 

FullSceneObject* StaticSceneObject::CreateSceneObject(FullSceneObject* parent, std::vector<Geometry> geometry, const char* diffuseTex)
{
	for (auto it = geometry.begin(); it != geometry.end(); it++)
	{
		auto pChild = new ToonShadedObject();
		pChild->SetGeometry(*it);
		pChild->SetDiffuse(std::string("resource/models/") + diffuseTex);
		parent->AddChild(pChild);
	}

	return parent;
}

void StaticSceneObject::OnAttach()
{  
	const float m6a1Scale = 15.0f;
	const float m6a1Translate = 13.0f;
	const float baseScale = 1.0f;
	const float wallTranslate = 60.0f;
	const float wallXscale = 0.39f;
	const float wallYscale = 0.15f;
	const float wallZscale = 0.5f;

	auto baseGeometry = LoadGeometry("resource/models/", "plain.obj");
	auto m6a1 = LoadGeometry("resource/models/", "M6A1.obj");
	auto wall = LoadGeometry("resource/models/", "wall.obj");
	
	
	CreateSceneObject(this, baseGeometry, "floor.bmp");
	this->GetLocalTransformComponent()->SetScale(glm::vec3(baseScale, baseScale, baseScale));
	this->GetLocalTransformComponent()->SetTranslate(glm::vec3(0.0f, 0.0f, 0.0f));
	this->GetMaterialComponent()->SetSpecular(glm::vec3(0.7,0.7,0.7));

	auto staticObj = CreateSceneObject(m6a1, "Aichino22.DDS");
	staticObj->GetLocalTransformComponent()->SetScale(glm::vec3(m6a1Scale, m6a1Scale, m6a1Scale));
	staticObj->GetLocalTransformComponent()->SetTranslate(glm::vec3(m6a1Translate, -0.278643f*m6a1Scale, -m6a1Translate)); // 0.27... is lowerst point of plane
	staticObj->GetLocalTransformComponent()->SetRotate(glm::vec3(0.0f, -glm::pi<float>(), 0.0f));
	this->AddChild(staticObj);

	staticObj = CreateSceneObject(m6a1, "Aichino22.DDS");
	staticObj->GetLocalTransformComponent()->SetScale(glm::vec3(m6a1Scale, m6a1Scale, m6a1Scale));
	staticObj->GetLocalTransformComponent()->SetTranslate(glm::vec3(-m6a1Translate, -0.278643f*m6a1Scale, -m6a1Translate));
	staticObj->GetLocalTransformComponent()->SetRotate(glm::vec3(0.0f, -glm::pi<float>() / 2.0f, 0.0f));
	this->AddChild(staticObj);

	// wall behind
	staticObj = CreateSceneObject(wall, "wall_DIF.bmp");
	staticObj->GetLocalTransformComponent()->SetScale(glm::vec3(wallXscale, wallYscale, wallZscale));
	staticObj->GetLocalTransformComponent()->SetTranslate(glm::vec3(0, wallTranslate/2, wallTranslate));
	staticObj->GetLocalTransformComponent()->SetRotate(glm::vec3(-glm::pi<float>(), 0.0f, 0.0f));
	this->AddChild(staticObj);
	// wall left
	staticObj = CreateSceneObject(wall, "wall_DIF.bmp");
	staticObj->GetLocalTransformComponent()->SetScale(glm::vec3(wallXscale, wallYscale, wallZscale));
	staticObj->GetLocalTransformComponent()->SetTranslate(glm::vec3(wallTranslate, wallTranslate/2, 0));
	staticObj->GetLocalTransformComponent()->SetRotate(glm::vec3(-glm::pi<float>(), -glm::pi<float>() / 2.0f, 0.0f));
	this->AddChild(staticObj);
	// wall right
	staticObj = CreateSceneObject(wall, "wall_DIF.bmp");
	staticObj->GetLocalTransformComponent()->SetScale(glm::vec3(wallXscale, wallYscale, wallZscale));
	staticObj->GetLocalTransformComponent()->SetTranslate(glm::vec3(-wallTranslate, wallTranslate/2, 0));
	staticObj->GetLocalTransformComponent()->SetRotate(glm::vec3(-glm::pi<float>(), glm::pi<float>() / 2.0f, 0.0f));
	this->AddChild(staticObj);

}

std::vector<Geometry> StaticSceneObject::LoadGeometry(const char* dir, const char* filename)
{
	auto meshes = SceneLoader::getInstance().Load(dir, filename);
	std::vector<Geometry> geometry;
	for (auto it = meshes.begin(); it != meshes.end(); it++)
	{
		Geometry geom;

		geom.AddIndices((*it).indices.data(), (*it).indices.size());

		for (unsigned int i = 0; i < (*it).vertices.size(); i++)
		{
			geom.AddVertex((*it).vertices[i]);

			if (!(*it).normals.empty())
				geom.AddNormal((*it).normals[i]);

			if (!(*it).texCoords.empty())
				geom.AddTexturCoord((*it).texCoords[i]);
		}
		geometry.push_back(geom);
	}

	return geometry;
}
