#ifndef KeyboardHandler_H
#define KeyboardHandler_H

#include "InputHandler.h"

class KeyboardHandler : public virtual InputHandler
{
public:
	KeyboardHandler()
	{
		Application::getInstance().AddHandler(this);
		Reset();
	}

	virtual void Reset() 
	{
		for (int i = 0; i < sizeof(mKeys)/sizeof(mKeys[0]); i++) 
		{
			mKeys[i] = false;
		}
	}

	virtual void OnKeyDown(unsigned char key)
	{
		mKeys[key] = true;
	}

	virtual void OnKeyUp(unsigned char key)
	{
		mKeys[key] = false;
	}

	bool IsKeyDown(unsigned char key)
	{
		return mKeys[key];
	}

private:
	bool mKeys[256];
};

#endif
