#ifndef Header_h__
#define Header_h__

class InputHandler
{
public:
	InputHandler()
	{
		mEnabled = true;
	}

	void Disable()
	{
		mEnabled = false;
	}

	void Enable()
	{
		mEnabled = true;
	}

	bool IsEnabled() { return mEnabled; }

private:
	bool mEnabled;
};

#endif // Header_h__
