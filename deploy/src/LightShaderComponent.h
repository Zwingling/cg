#ifndef LightShaderComponent_h__
#define LightShaderComponent_h__

#include "ShaderComponent.h"

class LightShaderComponent : public ShaderComponent
{
private:
	ShaderVariable mTextureSamplerDiffuseSV;
	ShaderVariable mTextureSamplerSpecularSV;
	ShaderVariable mTextureSamplerNormalSV;
	ShaderVariable mColorSV;

	ShaderVariable mAmbientSV;
	ShaderVariable mSpecularSV;
	ShaderVariable mDiffuseSV;

	ShaderVariable mMVPSV;
	ShaderVariable mViewSV;
	ShaderVariable mModelSV;
	ShaderVariable mModel3x3SV;
	ShaderVariable mTextureSamplerSV;
	ShaderVariable mLightColor;

	struct LightShaderVariable
	{
		ShaderVariable position;
		ShaderVariable color;
	};

	enum { MaxNumberOfLights = 8 };
	LightShaderVariable mLights[MaxNumberOfLights];

	ShaderVariable mNumberOfLights;
	ShaderVariable mLightPosition;

public:
	LightShaderComponent(FullSceneObject* owner);

	virtual void OnInit();
	virtual void Bind();
};


#endif // LightShaderComponent_h__