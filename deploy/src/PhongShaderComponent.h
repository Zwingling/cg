#ifndef PhongShaderComponent_h__
#define PhongShaderComponent_h__

#include "ShaderComponent.h"

class PhongShaderComponent : public ShaderComponent
{
private:
	ShaderVariable mTextureSamplerDiffuseSV;
	ShaderVariable mTextureSamplerSpecularSV;
	ShaderVariable mTextureSamplerNormalSV;
	ShaderVariable mColorSV;

	ShaderVariable mAmbientSV;
	ShaderVariable mSpecularSV;
	ShaderVariable mDiffuseSV;

	ShaderVariable mMVPSV;
	ShaderVariable mViewSV;
	ShaderVariable mModelSV;
	ShaderVariable mModel3x3SV;
	ShaderVariable mTextureSamplerSV;

	struct LightShaderVariable
	{
		ShaderVariable position;
		ShaderVariable color;
	};

	enum { MaxNumberOfLights = 8 };
	LightShaderVariable mLights[MaxNumberOfLights];

	ShaderVariable mNumberOfLights;
	ShaderVariable mLightPosition;

public:
	PhongShaderComponent(FullSceneObject* owner);

	virtual void OnInit();
	virtual void Bind();
};


#endif // PhongShaderComponent_h__
