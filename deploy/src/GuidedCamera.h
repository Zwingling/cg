#ifndef GuidedCamera_h__
#define GuidedCamera_h__

#include "Camera.h"
#include "CameraPathAnimation.h"

class GuidedCamera : public Camera, public KeyboardHandler
{
public:
	GuidedCamera(std::string name) : Camera(name.c_str())
	{
		mPathAnimation = AddComponent<CameraPathAnimation>(this);
	}

	CameraPathAnimation* GetPathAnimation() const { return mPathAnimation; }

	virtual void Disable()
	{
		InputHandler::Disable();
	}

	virtual void Enable()
	{
		InputHandler::Enable();
	}

	virtual void OnKeyDown(unsigned char key)
	{
		if (key == 'b')
		{
			if (mPathAnimation->IsRunning())
			{
				mPathAnimation->Pause();
			}
			else
			{
				mPathAnimation->Resume();
			}
		}
		else if (key == 'r')
		{
			mPathAnimation->Start();
		}
	}



private:
	CameraPathAnimation* mPathAnimation;
};


#endif // GuidedCamera_h__
