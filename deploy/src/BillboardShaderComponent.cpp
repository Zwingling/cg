#include "BillboardShaderComponent.h"
#include "Application.h"
#include "Camera.h"
#include "Scene.h"
#include "Light.h"

BillboardShaderComponent::BillboardShaderComponent(FullSceneObject* owner)
	: ShaderComponent("Billboard Shader Component", owner, "resource/shaders/Phong.vertexshader", "resource/shaders/StandardTransparentShading.fragmentshader")
{}

void BillboardShaderComponent::OnInit()
{
	ShaderComponent::OnInit();

	mTextureSamplerDiffuseSV = GetShaderVariable("DiffuseTextureSampler");
	mTextureSamplerSpecularSV = GetShaderVariable("SpecularTextureSampler");
	mTextureSamplerNormalSV = GetShaderVariable("NormalTextureSampler");
	mColorSV = GetShaderVariable("MeshColor");
	mAmbientSV = GetShaderVariable("ambientColor");
	mSpecularSV = GetShaderVariable("specularColor");
	mDiffuseSV = GetShaderVariable("diffuseColor");
	mMVPSV = GetShaderVariable("MVP");
	mViewSV = GetShaderVariable("V");
	mModelSV = GetShaderVariable("M");
	mModel3x3SV = GetShaderVariable("MV3x3");
	mLightPosition = GetShaderVariable("LightPosition_worldspace");
	mNumberOfLights = GetShaderVariable("numberOfLights");

	for (long long int i = 0; i < MaxNumberOfLights; i++)
	{
		mLights[i].position = GetShaderVariable(("lights[" + std::to_string(i) + "].position").c_str());
		mLights[i].color = GetShaderVariable(std::string("lights[" + std::to_string(i) + "].color").c_str());
	}

	const float MIN_RAND = 0.65, MAX_RAND = 0.9;
	const float range = MAX_RAND - MIN_RAND;
	randomThreshold = range * ((((float)rand()) / (float)RAND_MAX)) + MIN_RAND;
}

void BillboardShaderComponent::Bind()
{
	auto material = GetOwner()->GetMaterialComponent();
	auto scene = Application::getInstance().GetScene();
	auto pCamera = scene->GetActiveCamera();

	glm::mat4 mv = pCamera->GetView() *  GetOwner()->GetGlobalTransform();
	glm::mat4 mvp = pCamera->GetProjection() * mv;
	glm::mat4 view = pCamera->GetView();
	glm::mat4 model = GetOwner()->GetGlobalTransform();
	glm::mat3 mv3x3 = glm::mat3(mv);

	auto lights = scene->GetLights();
	std::vector<glm::vec3> lightsPos;
	int numberOfLights = lights.size() > MaxNumberOfLights ? MaxNumberOfLights : lights.size();

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	ShaderComponent::Bind();

	ShaderVariable v = GetShaderVariable("threshold");
	const float MIN_RAND = 0.0, MAX_RAND = 1.0;
	const float range = MAX_RAND - MIN_RAND;
	float change = range * ((((float)rand()) / (float)RAND_MAX)) + MIN_RAND;
	float changevalue = 0.0002;
	// clouds grow until threshold is reached
	if (randomThreshold > 0.9)
		changevalue *= -1;
	if (change > 0.5)
		randomThreshold += changevalue;

	v.SetFloat(randomThreshold);

	mMVPSV.SetMatrix(&mvp[0][0]);
	mViewSV.SetMatrix(&view[0][0]);
	mModelSV.SetMatrix(&model[0][0]);
	mModel3x3SV.SetMatrix3(&mv3x3[0][0]);

	mNumberOfLights.SetInteger(numberOfLights);
	for (int i = 0; i < numberOfLights; i++)
	{
		auto position = glm::vec3(lights[i]->GetGlobalTransform()[3]);
		auto color = glm::vec3(lights[i]->GetColor());

		mLights[i].position.SetVec3(position);
		mLights[i].color.SetVec3(color);
	}

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, material->GetDiffuseTexture());
	mTextureSamplerDiffuseSV.SetSampler(0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_2D, material->GetNormalTexture());
	mTextureSamplerNormalSV.SetSampler(1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_2D, material->GetSpecularTexture());
	mTextureSamplerSpecularSV.SetSampler(2);

	mColorSV.SetVec3(material->GetColor());
	mDiffuseSV.SetVec3(material->GetDiffuse());
	mAmbientSV.SetVec3(material->GetAmbient());
	mSpecularSV.SetVec3(material->GetSpecular());

}

void BillboardShaderComponent::Unbind()
{
	glDisable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
