=====================================================================
Team:
---------------------------------------------------------------------
Mathias H�lzl
Christoph Sch�pf
Florian Tischler

=====================================================================
Build:
---------------------------------------------------------------------
Zweimal cmake im deploy Verzeichnis ausf�hren (dort wo CMakeLists.txt liegt):

cmake ./
cmake ./

Anschlie�end mit make builden:

make

und ausf�hren:

./cg

=====================================================================
Steuerung:
---------------------------------------------------------------------
Maus f�r die Kameraorientierung
WASD    zum bewegen
F	f�r wechseln der Kamera (frei -> rotation -> gef�hrt -> frei -> ...)
G 	f�r wechseln der Rotationsrichtung bei der Rotationskamera
RT 	f�r speed kontrolle bei der Rotationskamera
Z 	f�r toggle zwischen Rotation um X Achse und Y Achse (bei Rotationskamera)
M   	f�r reset der Rotationskamera (position, richtung, speed und Rotationsachse wird zur�ckgesetzt)
B 	zum Pausieren der gef�hrten Kamera
R 	zum Neustarten des Pfades der gef�hrten Kamera
H	Hue des mittleren Lichts erh�hen
J	Hue des mittleren Lichts verringern
K	Value des mittleren Lichts erh�hen
L	Value des mittleren Lichts verringern
U	Toggle Diffuse Ein/Aus
I	Toggle Ambient Ein/Aus
O	Toggle Specular Ein/Aus

Um einen eigenen Pfad f�r die gef�hrte Kamera festzulegen in die freie Kamera wechseln
anschlie�end 'n' dr�cken um den aktuellen Pfad zu l�schen und dann mit 'p' neue Punkte einf�gen.
(mit der freien Kamera in die gew�nschte Position fahren 'p' dr�cken um zu speichern und dann die n�chste Position usw.)
